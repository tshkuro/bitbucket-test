// import { applyMiddleware, createStore } from 'redux';
// import rootReducer from './../src/reducers';
// import { middlewares } from './../src/createStore';

export const findByTestAttr = (component, attr) => {
    return component.find(`[datatest='${attr}']`);
}

export const mockFetchSuccess = (data) => {
    return jest.fn().mockImplementation(() => 
    Promise.resolve({
        status: 200,
        json: () => data
    }));
}

export const mockFetchError = () => {
    return jest.fn().mockImplementation(() => 
    Promise.reject('error'));
}

// export const testStore = (initialState) => {
//     const createStoreWithMiddleware = applyMiddleware(...middlewares)(createStore);
//     return createStoreWithMiddleware(rootReducer, initialState);
// };