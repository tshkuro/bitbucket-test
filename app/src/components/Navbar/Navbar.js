import React from 'react';
import { Link } from "react-router-dom";

import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavItem from 'react-bootstrap/NavItem';

export function TasksNavLink(props) {
    return (
        <NavItem>
            <Link datatest={props.id} onClick={() => props.onClick(props.link.id)} to={props.link.link} id={props.link.id} >{props.link.name}</Link>
        </NavItem>
    )
}

let TasksNavbar = (props) => {
    let items = props.links.map(link => <TasksNavLink id={link.id} key={link.id} link={link} onClick={props.onClick} />);
    return (
        <Navbar bg="light" expand="lg" className="my-margin-bottom">
            <Navbar.Brand href="#">{props.links[props.current].name}</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    {items}
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}

export default TasksNavbar;