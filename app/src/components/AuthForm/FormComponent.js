import React from "react";
import FormGroup from './FormGroup';
import './Form.css';

function SubmitResult() {
    return (
        <div className="submitSuccess" datatest="submitResult">
            Success!
        </div>
    );
}

const FormComponent = props => {
    return (
        <div className="container">
            <div className="row justify-content-center">
                <form className="inline-form">
                    <FormGroup
                        id="email"
                        type="email"
                        handleInput={props.handleInput}
                        title="Email"
                        datatestProp="emailInputComponent"
                        value={props.email.value}
                        classes={props.email.classes}
                        errorMessage={props.email.errorMessage}
                    />
                    <FormGroup
                        id="firstName"
                        type="text"
                        handleInput={props.handleInput}
                        title="First name"
                        datatestProp="firstNameInputComponent"
                        value={props.firstName.value}
                        classes={props.firstName.classes}
                        errorMessage={props.firstName.errorMessage}
                    />
                    <FormGroup
                        id="lastName"
                        type="text"
                        handleInput={props.handleInput}
                        title="Last name"
                        datatestProp="lastNameInputComponent"
                        value={props.lastName.value}
                        classes={props.lastName.classes}
                        errorMessage={props.lastName.errorMessage}
                    />
                    <button
                        type="submit"
                        className="btn btn-dark"
                        onClick={props.handleSubmit}
                        disabled={props.disabled}
                        datatest="submitButton"
                    >Submit</button>
                </form>
            </div>
            {props.submitted &&
                <div className="row justify-content-center" datatest="resultComponent">
                    <SubmitResult/>
                </div>
            }
        </div>
    );
}

export default FormComponent;