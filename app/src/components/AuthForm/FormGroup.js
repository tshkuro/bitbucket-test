import React from "react";
import './Form.css';

const ErrorMessage = props => (
    <span datatest="errorMessageComponent" id={props.id + "Message"}>{props.text}</span>
)

const FormGroup = props => {
    return (
        <div className="form-group" datatest={props.datatestProp}>
            <label htmlFor={props.id}>{props.title}</label>
            <input
                className={"form-control " + props.classes.join(' ')}
                type={props.type}
                name={props.type}
                id={props.id}
                onChange={props.handleInput}
                value={props.value}
            ></input>
            {props.errorMessage.length > 0 && <ErrorMessage id={props.id} text={props.errorMessage}/>}
        </div>
    );
}

export default FormGroup;