import React from "react";
import FormComponent from "./FormComponent"
import './Form.css';

class FormContainer extends React.Component {
    
    constructor() {
        super();        
        this.state = {
            firstName: {
                value: "",
                classes: [],
                errorMessage: ""
            },
            lastName: {
                value: "",
                classes: [],
                errorMessage: ""
            },
            email: {
                value: "",
                classes: [],
                errorMessage: ""
            },
            submitted: false,
        };
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            firstName: {
                value: "",
                classes: [],
                errorMessage: ""
            },
            lastName: {
                value: "",
                classes: [],
                errorMessage: ""
            },
            email: {
                value: "",
                classes: [],
                errorMessage: ""
            },  
            submitted: true
        });
        setTimeout(() => {
            this.setState({ submitted: false });
        }, 3000);

    }

    handleInput = (e) => {
        const field = e.target;
        let stateField = this.state[field.id];
        stateField.value = field.value;
        this.setState({
            stateField
        });
        this.validateField(field.id);
    }

    setFieldClass(fieldName, className = "") {
        let stateField = this.state[fieldName];
        stateField.classes = [];
        if (className.length !== 0) {
            stateField.classes.push(className);
        }
        this.setState({
            stateField
        });
    }

    setMessage(fieldName, message) {
        let stateField = this.state[fieldName];
        stateField.errorMessage = message;
        if (message.length === 0) {
            this.setFieldClass(fieldName, "fieldCorrect");
        } else {
            this.setFieldClass(fieldName, "fieldWrong");
        }
    }

    isInvalid = () => {
        return this.validateEmail(this.state.email.value) !== "" ||
            this.validateName(this.state.firstName.value) !== "" ||
            this.validateName(this.state.lastName.value) !== "";
    }

    validateField(fieldName) {
        let msg = "";
        const value = this.state[fieldName].value;
        if (value.length === 0) {
            this.setFieldClass(fieldName, "");
        } else {
            if (fieldName === "firstName" ||
                fieldName === "lastName") {
                msg = this.validateName(value);
            } else if (fieldName === "email") {
                msg = this.validateEmail(value);
            }
            this.setMessage(fieldName, msg);
        }
    }

    validateName(text) {
        if (!/^[a-zA-Z]+$/.test(text)) {
            return "must contain only letters";
        }
        if (text.length < 3) {
            return "must be longer than 3 symbols";
        }
        return "";
    }

    validateEmail(text) {
        if (!/^[a-zA-Z0-9_]+@[a-zA-Z]+\.[a-z0-9]+$/.test(text)) {
            return "not an email";
        }
        return "";
    }

    render() {
        const isInvalid = this.isInvalid();
        return (
            <FormComponent
                handleSubmit={this.handleSubmit}
                handleInput={this.handleInput}
                submitted={this.state.submitted}
                disabled={isInvalid}
                email={this.state.email}
                firstName={this.state.firstName}
                lastName={this.state.lastName}
            />
        );
    }

}

export default FormContainer;