import React from 'react'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import calculator from '../../reducers/calculator';
import CalcContainer from "./CalcContainer";

class CalcApp extends React.Component {
    constructor(props) {
        super(props);
        this.store = createStore(calculator);
    }

    render() {
        return (
            <Provider store={this.store}>
                <CalcContainer />
            </Provider>
        );
    }
}

export default CalcApp;