import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    addSymbol,
    delSymbol,
    clearAll,
    evaluate
} from '../../actions/calcActions';
import "./Calculator.css";
import Buttons from './Buttons';
import buttons from '../../constants/calculatorButtons';

const mapStateToProps = state => ({
    field: state
})

const act = (id, text) => {
    switch (id) {
        case 'btnAC':
            return clearAll();
        case 'btnBS':
            return delSymbol();
        case 'btnEquals':
            return evaluate();
        default:
            return addSymbol(text);
    }
}

const mapDispatchToProps = dispatch => ({
    onAction: (id, text) => dispatch(act(id, text))
})

const CalcComponent = ({ field, onAction }) => {
    return (
        <div className="calculator" datatest="calculatorComponent">
            <input
                readOnly
                id="calcInput"
                type="text"
                className="form-control"
                value={field}
            ></input>
            <Buttons buttons={buttons} onAction={onAction} />
        </div>
    );
}

CalcComponent.propTypes = {
    field: PropTypes.string.isRequired,
    onAction: PropTypes.func.isRequired
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CalcComponent);