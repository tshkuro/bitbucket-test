import React from 'react';
import PropTypes from 'prop-types';
import Button from './Button';

const ButtonRow = ({ buttons, onAction }) => {
    let btns = buttons.map(btn =>
        <Button
            key={btn.id}
            id={btn.id}
            text={btn.text}
            onClick={() => onAction(btn.id, btn.text)}
        />);
    return (
        <div className="row">
            {btns}
        </div>
    );
}

ButtonRow.propTypes = {
    buttons: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.string.isRequired,
            text: PropTypes.string.isRequired
        }).isRequired
    ).isRequired,
    onAction: PropTypes.func.isRequired
}

export default ButtonRow;