import React from 'react';
import PropTypes from 'prop-types';

const Button = ({id, text, onClick}) => {
    return (
        <button
            id={id}
            className="btn btn-light calcBtn"
            onClick={onClick}
        >{text}
        </button>
    );
}

Button.propTypes = {
    id: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
}

export default Button;