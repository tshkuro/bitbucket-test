import React from 'react';
import PropTypes from 'prop-types';
import ButtonRow from './ButtonRow';

const Buttons = ({ buttons, onAction }) => {
    return (
        <div className="container">
            <ButtonRow buttons={buttons.slice(0, 4)} onAction={onAction} />
            <ButtonRow buttons={buttons.slice(4, 8)} onAction={onAction} />
            <ButtonRow buttons={buttons.slice(8, 12)} onAction={onAction} />
            <ButtonRow buttons={buttons.slice(12, 16)} onAction={onAction} />
            <ButtonRow buttons={buttons.slice(16)} onAction={onAction} />
        </div>
    );
}

Buttons.propTypes = {
    buttons: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.string.isRequired,
            text: PropTypes.string.isRequired
        }).isRequired
    ).isRequired,
    onAction: PropTypes.func.isRequired
}

export default Buttons;