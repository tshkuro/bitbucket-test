import React, { useState } from 'react';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import {
    getFormattedDate,
    getFormattedDay,
    getFormattedTime,
    capitalizeFirstLetter,
    getFormattedTemperature,
    getFormattedPressure,
    getFormattedWind,
    getFormattedPrecipitation
} from './WeatherFormatting';

const Header = ({ time, index }) => (
    <div className="row justify-content-center" datatest="tabHeader">
        {(index === 0)
            ? <h2>Сегодня {getFormattedDay(time)}</h2>
            : <h2>{capitalizeFirstLetter(getFormattedDay(time))}</h2>
        }
    </div>
)

const HourWidgets = ({ data }) => (
    <div className="container" datatest="panelRow">
        <div className="row justify-content-center">
            <Hour time={data["dt"]} />
            <Description
                description={data["weather"][0]["description"]}
                icon={data["weather"][0]["icon"]}
            />
        </div>
        <Widgets data={data} />
    </div>
)

const Hour = ({ time }) => (
    <div
        className="col-md-2"
        datatest="timeWidget"
    >
        <h5>{getFormattedTime(time)}</h5>
    </div>
)

const Description = ({ description, icon }) => (
    <div
        className="col-md-10"
        datatest="descriptionWidget"
    >
        <h4>
            {capitalizeFirstLetter(description)}
            <img src={"http://openweathermap.org/img/w/" + icon + ".png"}></img>
        </h4>
    </div>
)

const Widgets = ({ data }) => (
    <div className="row">
        <TemperatureWidget temp={data["main"]["temp"]} />
        <PressureWidget pres={data["main"]["pressure"]} />
        {"wind" in data ? <WindWidget wind={data["wind"]} /> : null}
    </div>
)

const TemperatureWidget = ({ temp }) => (
    <div className="col-md col-sm" datatest="temperatureWidget">
        <h5>Температура</h5>
        <p>{`t = ${getFormattedTemperature(temp)}`}</p>
    </div>
)

const PressureWidget = ({ pres }) => (
    <div className="col-md col-sm" datatest="pressureWidget">
        <h5>Давление</h5>
        <p>{getFormattedPressure(pres)}</p>
    </div>
)

const WindWidget = ({ wind }) => (
    <div className="col-md col-sm" datatest="windWidget">
        <h5>Ветер</h5>
        <p>{getFormattedWind(wind)}</p>
    </div>
)

const DayTab = ({ data, index }) => {
    return (
        <div className="container days-panel" datatest="dayTab">
            <Header time={data[0]["dt"]} index={index} />
            {data.map(hourData => <HourWidgets data={hourData} />)}
        </div>
    )
}

const DayTabs = ({ data }) => {
    const [key, setKey] = useState(0);

    return (
        <Tabs
            activeKey={key}
            onSelect={k => setKey(k)}
            datatest="dayTabHeader"
        >
            {Object.keys(data).map((day, index) => (
                <Tab
                    eventKey={index}
                    title={day}
                >
                    <DayTab data={data[day]} index={index} />
                </Tab>
            ))}
        </Tabs >
    );
}

const FiveDaysForecastView = ({ data }) => (
    <div className="container" datatest="fiveDaysForecastView">
        <DayTabs data={data} />
    </div>
)

export default FiveDaysForecastView;