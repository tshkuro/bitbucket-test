import React from 'react';
import ReactLoading from 'react-loading';

const LoadingWidget = () => (
    <ReactLoading
        type={"bubbles"}
        color={"rgb(189, 251, 253)"}
    />
)

export default LoadingWidget;