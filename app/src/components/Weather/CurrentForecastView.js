import React from 'react';
import {
    getFormattedDay,
    getFormattedTime,
    capitalizeFirstLetter,
    getFormattedTemperature,
    getFormattedPressure,
    getFormattedWind,
    getFormattedPrecipitation
} from './WeatherFormatting';

const Header = ({ time }) => (
    <>
        <div className="row justify-content-center">
            <h2>Сегодня {getFormattedDay(time)}</h2>
        </div>
        <div className="row justify-content-center">
            <h4>Текущее время: {getFormattedTime(time)}</h4>
        </div>
    </>
)

const Description = ({ description, icon }) => (
    <div className="row justify-content-center">
        <h3>
            {capitalizeFirstLetter(description)}
            <img src={"http://openweathermap.org/img/w/" + icon + ".png"}></img>
        </h3>
    </div>
)

const Widgets = ({ data }) => (
    <div className="row">
        <TemperatureWidget
            temp={data["main"]["temp"]}
            tempMin={data["main"]["temp_min"]}
            tempMax={data["main"]["temp_max"]}
        />
        <PressureWidget pres={data["main"]["pressure"]} />
        {"wind" in data ? <WindWidget wind={data["wind"]} /> : null}
        {"rain" in data ? <RainWidget rain={data["rain"]} /> : null}
        {"snow" in data ? <SnowWidget snow={data["snow"]} /> : null}


    </div>
)

const TemperatureWidget = ({ temp, tempMin, tempMax }) => (
    <div className="col-md" datatest="temperatureWidget">
        <h5>Температура</h5>
        <p>{`t = ${getFormattedTemperature(temp)}`}</p>
        <p>{`мин. t = ${getFormattedTemperature(tempMin)}`}</p>
        <p>{`макс. t = ${getFormattedTemperature(tempMax)}`}</p>
    </div>
)

const PressureWidget = ({ pres }) => (
    <div className="col-md" datatest="pressureWidget">
        <h5>Давление</h5>
        <p>{getFormattedPressure(pres)}</p>
    </div>
)

const WindWidget = ({ wind }) => (
    <div className="col-md" datatest="windWidget">
        <h5>Ветер</h5>
        <p>{getFormattedWind(wind)}</p>
    </div>
)

const RainWidget = ({ rain }) => (
    <div className="col-md" datatest="rainWidget">
        <h5>Дождь</h5>
        <p>{getFormattedPrecipitation(rain["rain.3h"])}</p>
    </div>
)

const SnowWidget = ({ snow }) => (
    <div className="col-md" datatest="snowWidget">
        <h5>Снег</h5>
        <p>{getFormattedPrecipitation(snow["3h"])}</p>
    </div>
)

const CurrentForecastView = ({ data }) => {
    return (
        <div className="container cur-panel" datatest="currentForecastView">
            <Header time={data['dt']} />
            <Description
                description={data['weather'][0]['description']}
                icon={data['weather'][0]['icon']}
            />
            <Widgets data={data} />

        </div>
    );

}

export default CurrentForecastView;
