export const getWindDir = (degree) => {
    let dir;
    if (degree === 0 && degree === 360) {
        // dir = "С " + String.fromCharCode(0x2191);
        dir = String.fromCharCode(0x2191) + " (c)";
    }
    else if (0 < degree && degree < 90) {
        // dir = "СВ " + String.fromCharCode(0x2197);
        dir = String.fromCharCode(0x2197) + " (св)";
    }
    else if (degree === 90) {
        // dir = "В " + String.fromCharCode(0x2192);
        dir = String.fromCharCode(0x2192) + " (в)";
    }
    else if (90 < degree && degree < 180) {
        // dir = "ЮВ " + String.fromCharCode(0x2198);
        dir = String.fromCharCode(0x2198) + " (юв)";
    }
    else if (degree === 180) {
        // dir = "Ю " + String.fromCharCode(0x2193);
        dir = String.fromCharCode(0x2193) + " (ю)";
    }
    else if (180 < degree && degree < 270) {
        // dir = "ЮЗ " + String.fromCharCode(0x2199);
        dir = String.fromCharCode(0x2199) + " (юз)";
    }
    else if (degree === 270) {
        // dir = "З " + String.fromCharCode(0x2190);
        dir = String.fromCharCode(0x2190) + " (з)";
    }
    else {
        // dir = "СЗ " + String.fromCharCode(0x2196);
        dir = String.fromCharCode(0x2196) + " (сз)";
    }
    return dir;
}

export const getFormattedFullDate = (time) => {
    let options = {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        weekday: 'long',
        timezone: 'UTC',
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
    };
    let date = new Date(time * 1000);
    return date.toLocaleString("ru", options);
}

export const getFormattedDay = (time) => {
    let options = {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        weekday: 'long',
    };
    let date = new Date(time * 1000);
    return date.toLocaleString("ru", options);
}

export const getFormattedDate = (time) => {
    let options = {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
    };
    let date = new Date(time * 1000);
    return date.toLocaleString("ru", options);
}

export const getFormattedTime = (time) => {
    let options = {
        hour: 'numeric',
        minute: 'numeric'
    };
    let date = new Date(time * 1000);
    return date.toLocaleString("ru", options);
}

export const capitalizeFirstLetter = (string) =>
    string.charAt(0).toUpperCase() + string.slice(1)

export const getFormattedTemperature = (temp) => `${temp}C\xB0`

export const getFormattedPressure = (pres) => `${(pres / 1.333).toFixed(2)} мм.рт.ст.`

export const getFormattedWind = ({ speed, deg }) => `${speed} м/с ${getWindDir(deg)}`

export const getFormattedPrecipitation = (value) => {
    return `За последние 3 часа: ${value} мм`
}