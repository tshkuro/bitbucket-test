import React from 'react';
import { connect } from 'react-redux';
import SelectCities from './SelectCities';
import "./Weather.css";

const mapStateToProps = state => ({
    selectedCity: state.selectedCity
})

const Header = ({ selectedCity }) => {
    return (
        <div className="container" datatest="weatherHeaderComponent">
            <div className={
                `row 
                jumbotron 
                weather-header 
                justify-content-center`}>
                <h1 className="display-5">What is the weather?</h1>
            </div>
            <div className="row">
                <SelectCities />
            </div>
            <div className="row">
                <h3>{selectedCity.name}</h3>
            </div>
        </div>
    )
}

export default connect(
    mapStateToProps
)(Header)