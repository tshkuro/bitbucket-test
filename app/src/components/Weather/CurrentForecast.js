import React from 'react';
import { connect } from 'react-redux';
import {
    fetchCurrentIfNeeded,
    receiveCurrent
} from '../../actions/weatherActions';
import CurrentForecastView from './CurrentForecastView';
import LoadingWidget from './LoadingWidget';

const mapStateToProps = state => ({
    forecast: state.currentForecast,
    selectedCity: state.selectedCity
})

const mapDispatchToProps = dispatch => ({
    fetch: () => dispatch(fetchCurrentIfNeeded()),
    recieve: data => dispatch(receiveCurrent(data))
})


class Current extends React.Component {
    render() {
        let { forecast, selectedCity } = this.props;
        if (forecast.cityId !== selectedCity.id) {
            this.props.fetch();
        }
        let content = forecast.cityId === selectedCity.id && forecast.cityId !== -1
            ? forecast.isFetching
                ? <LoadingWidget />
                : forecast.didInvalidate
                    ? <div>Some error occured</div>
                    : <CurrentForecastView data={forecast.data} />
            : null
        return (
            <div>
                {content}
            </div>
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Current);