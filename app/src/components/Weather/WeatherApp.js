import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { Provider } from 'react-redux';
import Header from './Header';
import WeatherPanel from './WeatherPanel';
import weather from '../../reducers/weather';

class WeatherApp extends React.Component {
    constructor(props) {
        super(props);
        this.store = createStore(
            weather,
            applyMiddleware(
                thunkMiddleware
            )
        );
    }
    render() {
        return (
            <Provider store={this.store}>
                <Header />
                <WeatherPanel />
            </Provider>
        )
    }
}

export default WeatherApp;
