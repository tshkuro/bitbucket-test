import React from 'react';
import { connect } from 'react-redux';
import {
    fetchCitiesIfNeeded,
    receiveCities,
    selectCity
} from '../../actions/weatherActions';
import './Weather.css';
import LoadingWidget from './LoadingWidget';

const mapStateToProps = state => ({
    cities: state.cities,
    selectedCity: state.selectedCity,
    forecastType: state.forecastType
})

const mapDispatchToProps = dispatch => ({
    loadCities: () => dispatch(fetchCitiesIfNeeded()),
    recieve: data => dispatch(receiveCities(data)),
    selectCity: (id, text) => dispatch(selectCity(id, text))
})

const Loading = () => (
    <LoadingWidget />
)

class Select extends React.Component {
    state = {
        filter: ''
    }

    onInput = (e) => {
        this.setState({ filter: e.target.value });
    }

    filterCities = (cities) => {
        if (this.input && this.input.value !== '') {
            return cities.filter(city =>
                city.name.toLowerCase().
                    includes(this.input.value.toLowerCase()));
        }
        return cities;
    }

    render() {
        let { cities, selectCity } = this.props;
        return (
            <form className="ml-1" onSubmit={e => e.preventDefault()} datatest="citySearchComponent">
                <div className="form-row">
                    {/* <div className="col-md-4 mr-1"> */}
                    <input
                        onBlur={this.onInput}
                        defaultValue=""
                        className="form-control city-search mb-1"
                        ref={el => this.input = el}
                        type="text"
                        placeholder="Search..." />
                </div>
                <div className="form-row" datatest="citySelectComponent">
                    {/* <div className="col-md-2 offset-md-2"> */}
                    <select
                        className="form-control city-select"
                        onChange={e => selectCity(e.target.value,
                            e.target.options[e.target.selectedIndex].textContent)}>
                        <option value="" disabled selected>Select city</option>
                        {this.filterCities(cities.data)
                            .map(city =>
                                <option
                                    key={city.id}
                                    value={city.id}
                                >
                                    {city.name}
                                </option>)}
                    </select>
                </div>
                {/* </div> */}
            </form >
        )
    }
}

class SelectCities extends React.Component {
    render() {
        let { cities, loadCities, recieve } = this.props;
        if (cities.data.length === 0 && !cities.isFetching) {
            loadCities()
                .then(data => recieve(data));
        }

        let content = cities.data.length === 0
            ? <Loading />
            : <Select {...this.props} />;

        return (
            <div>
                {content}
            </div>
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SelectCities)
