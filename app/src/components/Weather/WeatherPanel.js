import React from 'react';
import { connect } from 'react-redux';
import CurrentForecast from './CurrentForecast';
import FiveDaysForecast from './FiveDaysForecast';
import { forecastTypes } from '../../constants/weatherTypes';

const mapStateToProps = state => ({
    selectedForecast: state.selectedForecast,
    selectedCity: state.selectedCity
})

const SelectType = ({ onSelect }) => (
    <form datatest="typeSelectComponent">
        <select
            onChange={onSelect}
            className="form-control mb-1"
            style={{ width: "300px" }}
        >
            <option value={forecastTypes.CURRENT} selected>Current</option>
            <option value={forecastTypes.FIVE_DAYS}>Five days</option>
        </select>
    </form>
)

class Panel extends React.Component {
    state = {
        selectedForecast: forecastTypes.CURRENT
    }

    onSelect = (e) => {
        let type = e.target.value;
        this.setState({ selectedForecast: type });
    }

    render() {
        const forecast = (this.state.selectedForecast === forecastTypes.CURRENT)
            ? <CurrentForecast />
            : <FiveDaysForecast />;
        return (
            <>
                <SelectType onSelect={this.onSelect} />
                {forecast}
            </>
        )
    }
}

export default connect(mapStateToProps)(Panel);