import React from 'react';
import { connect } from 'react-redux';
import {
    fetchFiveDaysIfNeeded,
    receiveFiveDays
} from '../../actions/weatherActions';
import FiveDaysForecastView from './FiveDaysForecastView';
import LoadingWidget from './LoadingWidget';

const mapStateToProps = state => ({
    forecast: state.fiveDaysForecast,
    selectedCity: state.selectedCity
})

const mapDispatchToProps = dispatch => ({
    fetch: () => dispatch(fetchFiveDaysIfNeeded()),
    recieve: data => dispatch(receiveFiveDays(data))
})

class FiveDays extends React.Component {
    render() {
        let { forecast, selectedCity } = this.props;
        if (forecast.cityId !== selectedCity.id) {
            this.props.fetch();
        }
        let content = forecast.cityId === selectedCity.id && forecast.cityId !== -1
            ? forecast.isFetching
                ? <LoadingWidget />
                : forecast.didInvalidate
                    ? <div>Some error occured</div>
                    : <FiveDaysForecastView data={forecast.data} />
            : null
        return (
            <div>
                {content}
            </div>
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FiveDays);