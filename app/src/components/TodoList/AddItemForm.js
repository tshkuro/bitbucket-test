import React from "react";
import { connect } from 'react-redux';
import { addTodo } from '../../actions/todoActions';
import "./Todolist.css";

const AddItemForm = ({ dispatch }) => {
    let input;

    const onClick = e => {
        e.preventDefault();
        if (!input.value) {
            return;
        }
        dispatch(addTodo(input.value));
        input.value = '';
    }

    return (
        <div className="row justify-content-center">
            <form className="form-inline">
                <input
                    datatest="addItemInput"
                    ref={node => { input = node }}
                    className="form-control"
                    type="text"
                    placeholder="New item..." />
                <button
                datatest="addItemButton"
                    className="btn btn-secondary my-margin-left"
                    onClick={onClick}
                >Add</button>
            </form>
        </div>
    );
}

export default connect()(AddItemForm);