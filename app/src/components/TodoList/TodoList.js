import React from "react";
import TodoItem from './TodoItem';
import PropTypes from 'prop-types';
import "./Todolist.css";

const TodoList = ({ todos, toggleTodo, deleteTodo }) => {
    const items = todos.map(todo =>
        <TodoItem
            key={todo.id}
            todo={todo}
            toggleTodo={() => toggleTodo(todo.id)}
            deleteTodo={() => deleteTodo(todo.id)}
        />);
    return (
        <div className="row">
            <div className="container">
                <div className="todo-list">
                    <div className="row">
                        <h3>TODO list</h3>
                    </div>
                    <div className="row">
                        {items}
                    </div>
                </div>
            </div>
        </div>
    );
}

TodoList.propTypes = {
    todos: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            text: PropTypes.string.isRequired,
            completed: PropTypes.bool.isRequired
        }).isRequired
    ).isRequired,
    toggleTodo: PropTypes.func.isRequired,
    deleteTodo: PropTypes.func.isRequired
}

export default TodoList;