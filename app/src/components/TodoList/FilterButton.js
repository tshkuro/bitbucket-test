import React from 'react';
import PropTypes from 'prop-types';

const FilterButton = ({ active, children, onClick }) => (
    <button
        onClick={onClick}
        disabled={active}
        className="btn btn-light ml-1"
    >
        {children}
    </button>
)

FilterButton.propTypes = {
    active: PropTypes.bool.isRequired,
    children: PropTypes.node,
    onClick: PropTypes.func.isRequired
}

export default FilterButton;
