import React from "react";
import PropTypes from 'prop-types';
import "./Todolist.css";

const TodoItem = ({ todo, toggleTodo, deleteTodo }) => {
    const completedStyle = {
        color: "gray",
        textDecoration: "line-through"
    }
    let checked = todo.completed;
    return (
        <div className="todo-item" datatest="todoItemComponent">
            <div className="col-md">
                <input
                    type="checkbox"
                    id={`todoItem${todo.id}`}
                    className="form-check-input"
                    checked={checked}
                    onChange={toggleTodo} />
            </div>
            <div className="col-md">
                <label
                    className="form-check-label"
                    style={todo.completed ? completedStyle : null}
                    htmlFor={`todoItem${todo.id}`}
                >{todo.text}</label>
            </div>
            <div className="col-md">
                <button
                    className="deleteBtn"
                    onClick={deleteTodo}
                >x</button>
            </div>
        </div>
    )
}

TodoItem.propTypes = {
    todo: PropTypes.shape({
        text: PropTypes.string.isRequired,
        completed: PropTypes.bool.isRequired,
        id: PropTypes.number.isRequired
    }).isRequired,
    toggleTodo: PropTypes.func.isRequired,
    deleteTodo: PropTypes.func.isRequired
}

export default TodoItem;