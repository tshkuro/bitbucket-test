import React from 'react';
import FilterLink from './FilterLink';
import filters from '../../constants/filters';

const Filters = () => (
    <div className="filters">
        <span>Show: </span>
        <FilterLink filter={filters.SHOW_ALL}>
            All
        </FilterLink>
        <FilterLink filter={filters.SHOW_ACTIVE}>
            Active
        </FilterLink>
        <FilterLink filter={filters.SHOW_COMPLETED}>
            Completed
        </FilterLink>
    </div >
)

export default Filters;