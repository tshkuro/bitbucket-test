import React from 'react'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import AddItemForm from './AddItemForm'
import VisibleTodoList from './VisibleTodoList'
import Filters from './Filters'
import todolist from '../../reducers/todolist'

class TodoListApp extends React.Component {
    constructor(props) {
        super(props);
        this.store = createStore(todolist);
    }
    render() {
        return (
            <Provider store={this.store}>
                <Filters />
                <VisibleTodoList />
                <AddItemForm />
            </Provider>
        )
    }
}

export default TodoListApp;