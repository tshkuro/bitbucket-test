const todoData = [
    {
        id: 1,
        text: "Take out the trash",
        completed: true
    },
    {
        id: 2,
        text: "Cook dinner",
        completed: false
    },
    {
        id: 3,
        text: "Feed the cat",
        completed: false
    },
    {
        id: 4,
        text: "Do homework",
        completed: false
    }
]

export default todoData