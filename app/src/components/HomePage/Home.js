import React from 'react';

function Home() {
    return (
        <div>
            <div className="row justify-content-center">
                <h3>Welcome to React learning app</h3><br />
            </div>
            <div className="row justify-content-center">
                <h4>You can select one of the tasks in the navbar at the top.</h4>
            </div>
        </div>
    );
}

export default Home;