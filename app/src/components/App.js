import React from 'react';
import { Route, Switch } from "react-router-dom";
import './App.css';
import Home from "./HomePage/Home";
import TasksNavbar from "./Navbar/Navbar";
import TodoListApp from "./TodoList/TodoListApp";
import FormContainer from "./AuthForm/FormContainer";
import CalcApp from "./Calculator/CalcApp";
import WeatherApp from './Weather/WeatherApp';

export const NotFound = () => {
  let style = {
    margin: "10%",
    backgroundColor: "lightgrey",
    padding: "5%",
    borderRadius: "10px"
  };
  return (
    <div className="row justify-content-center">
      <h3 style={style}>Oooops! There's nothing in here</h3>
    </div>
  );
}

export const Main = () => (
  <main>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/auth" component={FormContainer} />
      <Route path="/todo" component={TodoListApp} />
      <Route path="/calc" component={CalcApp} />
      <Route path="/weather" component={WeatherApp} />
      <Route component={NotFound} />
    </Switch>
  </main>
)

class App extends React.Component {
  constructor() {
    super();
    this.links = [
      { name: "Home", component: <Main />, id: "mainPage", link: "/" },
      { name: "Authentication Form", component: <FormContainer />, id: "task1", link: "/auth" },
      { name: "Todo List", component: <TodoListApp />, id: "task2", link: "/todo" },
      { name: "Calculator", component: <CalcApp />, id: "task3", link: "/calc" },
      { name: "Weather", component: <WeatherApp />, id: "task4", link: "/weather" },
    ];
    this.state = {
      current: 0
    };
    this.handleNavLinkClick = this.handleNavLinkClick.bind(this);
  }

  handleNavLinkClick(id) {
    this.setState(() => {
      let current = this.links.findIndex(link => link.id === id);
      return { current };
    });
  }

  render() {
    return (
      <div className="container">
        <TasksNavbar
          links={this.links}
          current={this.state.current}
          onClick={this.handleNavLinkClick} />
        <Main />
      </div>
    );
  }
}

export default App;
