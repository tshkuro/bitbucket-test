import * as actions from '../../../actions/todoActions';
import filters from '../../../constants/filters';
import types from '../../../constants/todolistTypes';

describe('todolist actions', () => {
    it('should create an action to add a todo', () => {
        const text = 'Some todo';
        const expectedAction = {
            id: 0,
            type: types.ADD_ITEM,
            text
        }
        expect(actions.addTodo(text)).toEqual(expectedAction);
    });
    it('should create an action to delete a todo', () => {
        const id = 0;
        const expectedAction = {
            type: types.DELETE_ITEM,
            id: 0
        }
        expect(actions.deleteTodo(id)).toEqual(expectedAction);
    });
    it('should create an action to toggle a todo', () => {
        const id = 0;
        const expectedAction = {
            type: types.TOGGLE_ITEM,
            id: 0
        }
        expect(actions.toggleTodo(id)).toEqual(expectedAction);
    });
    it('should create an action to filter todos', () => {
        let filter = filters.SHOW_ALL;
        let expectedAction = {
            type: types.FILTER,
            filter
        }
        expect(actions.setVisibilityFilter(filter)).toEqual(expectedAction);
    });
});