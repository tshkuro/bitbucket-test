import React from 'react';
import { shallow, mount } from 'enzyme';
import TodoItem from '../../TodoList/TodoItem';

describe("Todo item tests", () => {
    let wrapper;
    beforeEach(() => {
        wrapper = shallow(
            <TodoItem
                todo={{
                    id: 1,
                    text: "todo1",
                    completed: true
                }}
                toggleTodo={() => {}}
                deleteTodo={() => {}}
            />
        )
    });

    it('should render correctly', () => {
        // checkbox
        expect(wrapper.find('input')).toBeTruthy();
        expect(wrapper.find('input').length).toBe(1);

        // label
        expect(wrapper.find('label')).toBeTruthy();
        expect(wrapper.find('label').length).toBe(1);

        // delete button
        expect(wrapper.find('button')).toBeTruthy();
        expect(wrapper.find('button').length).toBe(1);        
    });
})