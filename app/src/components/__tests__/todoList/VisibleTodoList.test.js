import React from 'react';
import { shallow, mount, configure } from 'enzyme';
import configureStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import VisibleTodoList from '../../TodoList/VisibleTodoList';

const todos = [
    {
        id: 1,
        text: "todo1",
        completed: true
    },
    {
        id: 2,
        text: "todo2",
        completed: false
    }
];

class MockTodoList {
    todos = [...todos];

    toggleTodo = (id) => {
        this.todos = this.todos.map(todo => {
            if (todo.id === id) {
                todo.completed = !todo.completed;
            }
            return todo;
        })
    }

    deleteTodo = (id) => {
        this.todos = this.todos.filter(todo => todo.id !== id);
    }
}

describe('Visible TodoList tests', () => {
    // const initialState = { todos: [...todos] };
    // const mockStore = configureStore();
    // let store, wrapper, mockList;

    beforeEach(() => {
        // mockList = new MockTodoList();
        // store = mockStore(initialState);
        // wrapper = mount(
        //     <Provider store={store}>
        //         <VisibleTodoList />
        //     </Provider>
        // )
    });

    it('renders', () => {
        // console.log(wrapper.debug());
    })


})
