import React from 'react';
import { shallow, mount } from 'enzyme';
// import renderer from 'react-test-renderer';
import TodoList from '../../TodoList/TodoList';

class MockTodoList {
    todos = [
        {
            id: 1,
            text: "todo1",
            completed: true
        },
        {
            id: 2,
            text: "todo2",
            completed: false
        }
    ];
    toggleTodo = (id) => {
        this.todos = this.todos.map(todo => {
            if (todo.id === id) {
                todo.completed = !todo.completed;
            }
            return todo;
        })
    }

    deleteTodo = (id) => {
        this.todos = this.todos.filter(todo => todo.id !== id);
    }
}

describe('TodoList tests', () => {
    let wrapper;
    let mockList;

    beforeEach(() => {
        mockList = new MockTodoList();
        wrapper = shallow(<TodoList
            todos={mockList.todos}
            toggleTodo={mockList.toggleTodo}
            deleteTodo={mockList.deleteTodo}
        />);
    });

    it('should be rendered correctly', () => {
        expect(wrapper.find('TodoItem').length).toBe(2);
        // wrapper.find('TodoItem').forEach((todoItem) => {
        //     expect(todoItem.find('input'))
        //     console.log(todoItem.debug());

        // })
    });
});