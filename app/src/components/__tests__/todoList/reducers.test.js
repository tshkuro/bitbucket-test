import reducer from '../../../reducers/todolist';
import types from '../../../constants/todolistTypes';
import filters from '../../../constants/filters';
import todoData from '../../TodoList/todoData';

describe('todos reducer', () => {
    it('should return the initial state', () => {
        const expected = {
            todos: [
                ...todoData
            ],
            visibilityFilter: filters.SHOW_ALL
        };

        expect(reducer(undefined, {})).toEqual(expected);
    });

    it('should handle ADD_TODO', () => {
        const newTodo = {
            text: 'some todo',
            completed: false,
            id: undefined
        };
        const expected = {
            todos: [
                ...todoData,
                newTodo
            ],
            visibilityFilter: filters.SHOW_ALL
        };

        expect(reducer(undefined, {
            type: types.ADD_ITEM,
            text: 'some todo'
        })
        ).toEqual(expected);
    });
})