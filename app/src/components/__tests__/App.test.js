import React from 'react';
import { shallow } from 'enzyme';
import App, {NotFound, Main} from '../App';

describe('App test', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<App/>);
    });

    it('renders App', () => {
        expect(wrapper.find('TasksNavbar')).toBeTruthy();
        expect(wrapper.find('TasksNavbar').length).toBe(1);

        expect(wrapper.find('Main')).toBeTruthy();
        expect(wrapper.find('Main').length).toBe(1);        
    });

    it('handleNavLinkClick works', () => {
        const links = wrapper.instance().links;
        const k = 1;
        expect(wrapper.instance().state.current).toBe(0);
        wrapper.instance().handleNavLinkClick(links[k].id);
        expect(wrapper.instance().state.current).toBe(k);       
    });
});

describe('Main component test', () => {
    it('renders Main', () => {
        const wrapper = shallow(<Main/>);
        expect(wrapper.find('Switch')).toBeTruthy();
        expect(wrapper.find('Switch').length).toBe(1);
        expect(wrapper.find('Route')).toBeTruthy();
    });
});

describe('Not found component test', () => {
    it('renders NotFound', () => {
        const wrapper = shallow(<NotFound/>);
        expect(wrapper).toMatchSnapshot();
    });
});

