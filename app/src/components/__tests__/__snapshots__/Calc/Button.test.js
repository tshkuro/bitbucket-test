import React from 'react';
import { mount, shallow } from 'enzyme'
import Button from '../../../Calculator/Button';

const props = {
    id: '1',
    text: 'test',
    onClick: () => {}
}

describe('calc button test', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = shallow(
            <Button
                id={props.id}
                text={props.text}
                onClick={props.onClick}
            />
        );
    });

    it('should be rendered correctly', () => {
        const button = wrapper.find('button');
        expect(button).toBeTruthy();
        expect(button.text()).toBe(props.text);
        expect(button.prop('id')).toBe(props.id);
    });
})