import * as actions from '../../../../actions/calcActions';
import types from '../../../../constants/calculatorTypes';

describe('calc actions', () => {
    it('check action addSymbol', () => {
        const symbol = '1';
        const expectedAction = {
            type: types.ADD_SYMBOL,
            symbol
        }
        expect(actions.addSymbol(symbol)).toEqual(expectedAction);
    });

    it('check action delSymbol', () => {
        const expectedAction = {
            type: types.DEL_SYMBOL
        }
        expect(actions.delSymbol()).toEqual(expectedAction);
    });

    it('check action clearAll', () => {
        const expectedAction = {
            type: types.CLEAR_ALL
        }
        expect(actions.clearAll()).toEqual(expectedAction);
    });

    it('check action evaluate', () => {
        const expectedAction = {
            type: types.EVAL
        }
        expect(actions.evaluate()).toEqual(expectedAction);
    });
});