import React from 'react';
import { mount, shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import CalcContainer from '../../../Calculator/CalcContainer';
// import { findByTestAttr, testStore } from '../../../../../utils';
import { Provider } from 'react-redux';
import {addSymbol, delSymbol, clearAll, evaluate} from '../../../../actions/calcActions';

describe('CalcContainer test', () => {
    const initialState = '0';
    const mockStore = configureStore();
    let wrapper, store;

    beforeEach(() => {
        store = mockStore(initialState);
        wrapper = mount(
            <Provider store={store}>
                <CalcContainer />
            </Provider>
        );
    });

    it('should be rendered correctly', () => {
        // console.log(wrapper.debug());
        expect(wrapper.find('input')).toBeTruthy();
        expect(wrapper.find('input').length).toBe(1);
        expect(wrapper.find('Button')).toBeTruthy();
        expect(wrapper.find('Button').length).toBe(20);
        expect(wrapper.find('Buttons')).toMatchSnapshot();         
    });

    
});