import React from 'react';
import { mount, shallow } from 'enzyme'
import ButtonRow from '../../../Calculator/ButtonRow';

class MockProps {
    clicked = { id: null, text: null }
    buttons = [
        { id: '1', text: 'btn1'},
        { id: '2', text: 'btn2'}        
    ];
    onAction = (id, text) => {
        this.clicked.id = id;
        this.clicked.text = text;
    }
}

describe('Button Row test',() => {
    let wrapper;
    let mockProps;
    beforeEach(() => {
        mockProps = new MockProps();
        wrapper = shallow(
            <ButtonRow
                buttons={mockProps.buttons}
                onAction={mockProps.onAction}
            />
        );
    });

    it('should be rendered correctly',() => {
        expect(wrapper.find('Button')).toBeTruthy();
        expect(wrapper.find('Button').length).toBe(2);
    });
    
    it('onAction works properly with arguments',() => {
        let btn = wrapper.find('Button').at(0);
        btn.simulate('click');
        expect(mockProps.clicked.id).toBe(btn.prop('id'));
        expect(mockProps.clicked.text).toBe(btn.prop('text'));

    });
    
});