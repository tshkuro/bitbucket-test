import React from 'react';
import { mount, shallow } from 'enzyme';
import CalcApp from '../../../Calculator/CalcApp';

describe('CalcApp test', () => {
    it('renders correctly', () => {
        const wrapper = shallow(<CalcApp/>);
        expect(wrapper.find('CalcContainer')).toBeTruthy();
    });
})