import React from 'react';
import { shallow, mount } from 'enzyme';
import FormComponent from '../../AuthForm/FormComponent';
import FormContainer from '../../AuthForm/FormContainer';
import { findByTestAttr } from '../../../../utils';

// unit tests
describe('Shallow form component rendering', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = shallow(
            <FormComponent
                handleSubmit={()=> {}}
                handleInput={()=> {}}
                submitted={false}
                disabled={true}
                email={{ value: '', classes: [], errorMessage: ''}}
                firstName={{ value: '', classes: [], errorMessage: ''}}
                lastName={{ value: '', classes: [], errorMessage: ''}}
            />
        );
    });

    it('should render correctly', () => {
        expect(wrapper.find('form')).toBeTruthy();
        expect(wrapper.find('form').length).toBe(1);

        expect(wrapper.find('FormGroup')).toBeTruthy();
        expect(wrapper.find('FormGroup').length).toBe(3);

        expect(wrapper.find('button')).toBeTruthy();
        expect(wrapper.find('button').length).toBe(1);
    });
});

describe('Mount form container rendering', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = mount(<FormContainer />);
    });

    it('should render correct amount of items', () => {
        expect(findByTestAttr(wrapper, 'emailInputComponent')
            .length).toBe(1);

        expect(findByTestAttr(wrapper, 'firstNameInputComponent')
            .length).toBe(1);

        expect(findByTestAttr(wrapper, 'lastNameInputComponent')
            .length).toBe(1);

        expect(findByTestAttr(wrapper, 'submitButton')
            .length).toBe(1);
    });

    it('should render correct text', () => {
        expect(findByTestAttr(wrapper, 'emailInputComponent')
            .find('label').text()).toBe("Email");

        expect(findByTestAttr(wrapper, 'firstNameInputComponent')
            .find('label').text()).toBe("First name");

        expect(findByTestAttr(wrapper, 'lastNameInputComponent')
            .find('label').text()).toBe("Last name");

        expect(findByTestAttr(wrapper, 'submitButton')
            .text()).toBe("Submit");
    });

    it('submit should be disabled at first', () => {
        expect(findByTestAttr(wrapper, 'submitButton').instance().disabled).toBeTruthy();
        expect(wrapper.instance().isInvalid()).toBeTruthy();
    });

    it('input email should work', () => {
        const prevState = wrapper.instance().state;
        expect(prevState.email.value).toBe('');
        let emailInput = findByTestAttr(wrapper, 'emailInputComponent').find('input');
        emailInput.simulate('change', {target: { value: 'email', id: emailInput.prop('id')}});
        const newState = wrapper.instance().state;
        expect(newState.email.value).toBe('email');
    });

    it('input firstName should work', () => {
        const prevState = wrapper.instance().state;
        expect(prevState.firstName.value).toBe('');
        let firstNameInput = findByTestAttr(wrapper, 'firstNameInputComponent').find('input');
        firstNameInput.simulate('change', {target: { value: 'firstName', id: firstNameInput.prop('id')}});
        const newState = wrapper.instance().state;
        expect(newState.firstName.value).toBe('firstName');
    });

    //#region email field
    it('input lastName should work', () => {
        const prevState = wrapper.instance().state;
        expect(prevState.lastName.value).toBe('');
        let lastNameInput = findByTestAttr(wrapper, 'lastNameInputComponent').find('input');
        lastNameInput.simulate('change', {target: { value: 'lastName', id: lastNameInput.prop('id')}});
        const newState = wrapper.instance().state;
        expect(newState.lastName.value).toBe('lastName');
    });

    it('for invalid email error message should be shown', () => {
        const prevState = wrapper.instance().state;
        expect(prevState.email.classes.length).toBe(0);
        expect(prevState.email.errorMessage).toBe("");
        
        let emailInput = findByTestAttr(wrapper, 'emailInputComponent').find('input');
        emailInput.simulate('change', {target: { value: 'invalid email', id: emailInput.prop('id')}});
        const newState = wrapper.instance().state;
        
        wrapper.update();
        emailInput = findByTestAttr(wrapper, 'emailInputComponent').find('input');
        
        expect(newState.email.classes.length).toBe(1);
        expect(newState.email.classes).toContain('fieldWrong');
        expect(newState.email.errorMessage).toBe("not an email");
        expect(emailInput.prop('className')).toContain('fieldWrong');
        expect(findByTestAttr(wrapper, 'errorMessageComponent'));
    });

    it('for correct email field should have fieldCorrect class', () => {
        let emailInput = findByTestAttr(wrapper, 'emailInputComponent').find('input');
        emailInput.simulate('change', {target: { value: 'correct@email.com', id: emailInput.prop('id')}});
        const newState = wrapper.instance().state;
        
        wrapper.update();
        emailInput = findByTestAttr(wrapper, 'emailInputComponent').find('input');
        
        expect(newState.email.classes.length).toBe(1);
        expect(newState.email.classes).toContain('fieldCorrect');
    });

    it('when email is cleared it should not have any additional classes', () => {
        let emailInput = findByTestAttr(wrapper, 'emailInputComponent').find('input');
        emailInput.simulate('change', {target: { value: 'correct@email.com', id: emailInput.prop('id')}});
        emailInput.simulate('change', {target: { value: '', id: emailInput.prop('id')}});
        const newState = wrapper.instance().state;
        
        wrapper.update();
        emailInput = findByTestAttr(wrapper, 'emailInputComponent').find('input');
        
        expect(newState.email.classes.length).toBe(0);
    });
    //#endregion

    //#region first name field
    it('for invalid first name error message should be shown (must be longer than 3 symbols)', () => {
        const prevState = wrapper.instance().state;
        expect(prevState.firstName.classes.length).toBe(0);
        expect(prevState.firstName.errorMessage).toBe("");
        
        let firstNameInput = findByTestAttr(wrapper, 'firstNameInputComponent').find('input');
        firstNameInput.simulate('change', {target: { value: 'a', id: firstNameInput.prop('id')}});
        const newState = wrapper.instance().state;
        
        wrapper.update();
        firstNameInput = findByTestAttr(wrapper, 'firstNameInputComponent').find('input');
        
        expect(newState.firstName.classes.length).toBe(1);
        expect(newState.firstName.classes).toContain('fieldWrong');
        expect(newState.firstName.errorMessage).toBe("must be longer than 3 symbols");
        expect(firstNameInput.prop('className')).toContain('fieldWrong');
        expect(findByTestAttr(wrapper, 'errorMessageComponent'));
    });

    it('for invalid first name error message should be shown (must contain only letters)', () => {
        const prevState = wrapper.instance().state;
        expect(prevState.firstName.classes.length).toBe(0);
        expect(prevState.firstName.errorMessage).toBe("");
        
        let firstNameInput = findByTestAttr(wrapper, 'firstNameInputComponent').find('input');
        firstNameInput.simulate('change', {target: { value: 'a1234', id: firstNameInput.prop('id')}});
        const newState = wrapper.instance().state;
        
        wrapper.update();
        firstNameInput = findByTestAttr(wrapper, 'firstNameInputComponent').find('input');
        
        expect(newState.firstName.classes.length).toBe(1);
        expect(newState.firstName.classes).toContain('fieldWrong');
        expect(newState.firstName.errorMessage).toBe("must contain only letters");
        expect(firstNameInput.prop('className')).toContain('fieldWrong');
        expect(findByTestAttr(wrapper, 'errorMessageComponent'));
    });

    it('for correct first name field should have fieldCorrect class', () => {
        const prevState = wrapper.instance().state;
        expect(prevState.firstName.classes.length).toBe(0);
        expect(prevState.firstName.errorMessage).toBe("");
        
        let firstNameInput = findByTestAttr(wrapper, 'firstNameInputComponent').find('input');
        firstNameInput.simulate('change', {target: { value: 'CorrectFirstName', id: firstNameInput.prop('id')}});
        const newState = wrapper.instance().state;
        
        wrapper.update();
        firstNameInput = findByTestAttr(wrapper, 'firstNameInputComponent').find('input');
        
        expect(newState.firstName.classes.length).toBe(1);
        expect(newState.firstName.classes).toContain('fieldCorrect');
    });

    it('when first name field is cleared it should not have any additional classes', () => {
        let firstNameInput = findByTestAttr(wrapper, 'firstNameInputComponent').find('input');
        firstNameInput.simulate('change', {target: { value: 'CorrectFirstName', id: firstNameInput.prop('id')}});
        firstNameInput.simulate('change', {target: { value: '', id: firstNameInput.prop('id')}});
        const newState = wrapper.instance().state;
        
        wrapper.update();
        firstNameInput = findByTestAttr(wrapper, 'firstNameInputComponent').find('input');
        
        expect(newState.firstName.classes.length).toBe(0);
    });
    //#endregion

    //#region last name
    it('for invalid last name error message should be shown (must be longer than 3 symbols)', () => {
        const prevState = wrapper.instance().state;
        expect(prevState.lastName.classes.length).toBe(0);
        expect(prevState.lastName.errorMessage).toBe("");
        
        let lastNameInput = findByTestAttr(wrapper, 'lastNameInputComponent').find('input');
        lastNameInput.simulate('change', {target: { value: 'a', id: lastNameInput.prop('id')}});
        const newState = wrapper.instance().state;
        
        wrapper.update();
        lastNameInput = findByTestAttr(wrapper, 'lastNameInputComponent').find('input');
        
        expect(newState.lastName.classes.length).toBe(1);
        expect(newState.lastName.classes).toContain('fieldWrong');
        expect(newState.lastName.errorMessage).toBe("must be longer than 3 symbols");
        expect(lastNameInput.prop('className')).toContain('fieldWrong');
        expect(findByTestAttr(wrapper, 'errorMessageComponent'));
    });

    it('for invalid last name error message should be shown (must contain only letters)', () => {
        const prevState = wrapper.instance().state;
        expect(prevState.lastName.classes.length).toBe(0);
        expect(prevState.lastName.errorMessage).toBe("");
        
        let lastNameInput = findByTestAttr(wrapper, 'lastNameInputComponent').find('input');
        lastNameInput.simulate('change', {target: { value: 'a1234', id: lastNameInput.prop('id')}});
        const newState = wrapper.instance().state;
        
        wrapper.update();
        lastNameInput = findByTestAttr(wrapper, 'lastNameInputComponent').find('input');
        
        expect(newState.lastName.classes.length).toBe(1);
        expect(newState.lastName.classes).toContain('fieldWrong');
        expect(newState.lastName.errorMessage).toBe("must contain only letters");
        expect(lastNameInput.prop('className')).toContain('fieldWrong');
        expect(findByTestAttr(wrapper, 'errorMessageComponent'));
    });

    it('for correct last name field should have fieldCorrect class', () => {
        const prevState = wrapper.instance().state;
        expect(prevState.lastName.classes.length).toBe(0);
        expect(prevState.lastName.errorMessage).toBe("");
        
        let lastNameInput = findByTestAttr(wrapper, 'lastNameInputComponent').find('input');
        lastNameInput.simulate('change', {target: { value: 'CorrectLastName', id: lastNameInput.prop('id')}});
        const newState = wrapper.instance().state;
        
        wrapper.update();
        lastNameInput = findByTestAttr(wrapper, 'lastNameInputComponent').find('input');
        
        expect(newState.lastName.classes.length).toBe(1);
        expect(newState.lastName.classes).toContain('fieldCorrect');
    });

    it('when last name field is cleared it should not have any additional classes', () => {
        let lastNameInput = findByTestAttr(wrapper, 'lastNameInputComponent').find('input');
        lastNameInput.simulate('change', {target: { value: 'CorrectLastName', id: lastNameInput.prop('id')}});
        lastNameInput.simulate('change', {target: { value: '', id: lastNameInput.prop('id')}});
        const newState = wrapper.instance().state;
        
        wrapper.update();
        lastNameInput = findByTestAttr(wrapper, 'lastNameInputComponent').find('input');
        
        expect(newState.lastName.classes.length).toBe(0);
    });
    //#endregion

    jest.useFakeTimers();

    it('should be submitted if all fields are valid', () => {
        // fill in the form
        let emailInput = findByTestAttr(wrapper, 'emailInputComponent').find('input');
        emailInput.simulate('change', {target: { value: 'correct@email.com', id: emailInput.prop('id')}});
        
        let firstNameInput = findByTestAttr(wrapper, 'firstNameInputComponent').find('input');
        firstNameInput.simulate('change', {target: { value: 'CorrectFirstName', id: firstNameInput.prop('id')}});
        
        let lastNameInput = findByTestAttr(wrapper, 'lastNameInputComponent').find('input');
        lastNameInput.simulate('change', {target: { value: 'CorrectLastName', id: lastNameInput.prop('id')}});
        
        wrapper.update();

        // click submit button
        let button = findByTestAttr(wrapper, "submitButton");
        expect(button.prop('disabled')).toBeFalsy();
        button.simulate('click', { target: {}});
       
        wrapper.update();
        // it should show success message
        expect(findByTestAttr(wrapper, "resultComponent")).toBeTruthy();
        expect(findByTestAttr(wrapper, "resultComponent")).toMatchSnapshot(); 
        
        let state = wrapper.instance().state;
        expect(state.submitted).toBeTruthy();

        // after a timeout it success message should disappear 
        // and the submitted state should be set to false
        jest.advanceTimersByTime(4000);
        wrapper.update();

        state = wrapper.instance().state;
        expect(state.submitted).toBeFalsy();
    });
});

