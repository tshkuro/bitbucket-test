import React from 'react';
import { shallow, mount } from 'enzyme';
import Home from '../HomePage/Home';

describe('Home component', () => {
    it('renders correctly', () => {
        const wrapper = shallow(<Home/>);

        expect(wrapper).toMatchSnapshot();
        expect(wrapper.find('h3')).toBeTruthy();
        expect(wrapper.find('h4')).toBeTruthy();
    })
})