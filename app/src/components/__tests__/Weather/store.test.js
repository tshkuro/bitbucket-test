import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from '../../../actions/weatherActions';
import types from '../../../constants/weatherTypes';
import { mockFetchSuccess, mockFetchError } from '../../../../utils';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('async actions', () => {
    describe('current forecast actions', () => {
        it('creates RECEIVE_CURRENT when fetching current forecast has been done', () => {
            window.fetch = mockFetchSuccess({ forecast: 'test' });

            const expectedActions = [
                { type: types.REQUEST_CURRENT, cityId: 0 },
                { type: types.RECEIVE_CURRENT, data: { forecast: "test" } },
            ];
            const store = mockStore({
                currentForecast: {
                    isLoaded: false,
                    isFetching: false,
                    didInvalidate: false,
                    cityId: 0,
                    data: {}
                },
                selectedCity: { id: 0, name: 'test' },
            });

            store.dispatch(actions.fetchCurrentIfNeeded(store.dispatch, store.getState))
                .then(() => {
                    expect(store.getActions()).toEqual(expectedActions);
                    expect(fetch).toHaveBeenCalledTimes(1);
                });
        });

        it('creates INVALIDATE_CURRENT when error occured while fetching current forecast', () => {
            window.fetch = mockFetchError();

            const expectedActions = [
                { type: types.REQUEST_CURRENT, cityId: 0 },
                { type: types.INVALIDATE_CURRENT },
            ];
            const store = mockStore({
                currentForecast: {
                    isLoaded: false,
                    isFetching: false,
                    didInvalidate: false,
                    cityId: 0,
                    data: {}
                },
                selectedCity: { id: 0, name: 'test' },
            });
            store.dispatch(actions.fetchCurrentIfNeeded(store.dispatch, store.getState))
                .then(() => {
                    expect(store.getActions()).toEqual(expectedActions);
                    expect(fetch).toHaveBeenCalledTimes(1);
                });
        });

        it("does NOT fetch when it already has forecast needed", () => {
            window.fetch = mockFetchSuccess({});
            const store = mockStore({
                currentForecast: {
                    cityId: 0,
                    isFetching: false,
                    isLoaded: true,
                    didInvalidate: false,
                    data: {}
                },
                selectedCity: { id: 0, name: 'test' }
            });
            store.dispatch(actions.fetchCurrentIfNeeded(store.dispatch, store.getState))
                .then(() => {
                    expect(store.getActions()).toEqual([]);
                    expect(fetch).toHaveBeenCalledTimes(0);
                });
        });

        it("does NOT fetch when it is fetching forecast", () => {
            window.fetch = mockFetchSuccess({});
            const store = mockStore({
                currentForecast: {
                    cityId: 0,
                    isFetching: true,
                    isLoaded: false,
                    didInvalidate: false,
                    data: {}
                },
                selectedCity: { id: 0, name: 'test' }
            });
            store.dispatch(actions.fetchCurrentIfNeeded(store.dispatch, store.getState))
                .then(() => {
                    expect(store.getActions()).toEqual([]);
                    expect(fetch).toHaveBeenCalledTimes(0);
                });
        });
    });

    describe('five days forecast actions', () => {
        it('creates RECEIVE_DAYS when fetching five days forecast has been done ', () => {
            window.fetch = mockFetchSuccess({ forecast: 'test' });

            const expectedActions = [
                { type: types.REQUEST_DAYS, cityId: 0 },
                { type: types.RECEIVE_DAYS, data: { forecast: "test" } }
            ];
            const store = mockStore({
                selectedCity: { id: 0, name: 'test' },
                fiveDaysForecast: {
                    isLoaded: false,
                    isFetching: false,
                    didInvalidate: false,
                    cityId: 0,
                    data: {},
                }
            });
            store.dispatch(actions.fetchFiveDaysIfNeeded(store.dispatch, store.getState))
                .then(() => {
                    expect(store.getActions()).toEqual(expectedActions);
                    expect(fetch).toHaveBeenCalledTimes(1);
                });
        });

        it('creates INVALIDATE_DAYS when error occured while fetching five days forecast', () => {
            window.fetch = mockFetchError();

            const expectedActions = [
                { type: types.REQUEST_DAYS, cityId: 0 },
                { type: types.INVALIDATE_DAYS },
            ];
            const store = mockStore({
                selectedCity: { id: 0, name: 'test' },
                fiveDaysForecast: {
                    isLoaded: false,
                    isFetching: false,
                    didInvalidate: false,
                    cityId: 0,
                    data: {},
                }
            });
            store.dispatch(actions.fetchFiveDaysIfNeeded(store.dispatch, store.getState))
                .then(() => {
                    expect(store.getActions()).toEqual(expectedActions);
                    expect(fetch).toHaveBeenCalledTimes(1);
                });
        });

        it("does NOT fetch when it already has forecast needed", () => {
            window.fetch = mockFetchSuccess({});
            const store = mockStore({
                fiveDaysForecast: {
                    cityId: 0,
                    isFetching: false,
                    isLoaded: true,
                    didInvalidate: false,
                    data: {}
                },
                selectedCity: { id: 0, name: 'test' }
            });
            store.dispatch(actions.fetchFiveDaysIfNeeded(store.dispatch, store.getState))
                .then(() => {
                    expect(store.getActions()).toEqual([]);
                    expect(fetch).toHaveBeenCalledTimes(0);
                });
        });

        it("does NOT fetch when it is fetching forecast", () => {
            window.fetch = mockFetchSuccess({});
            const store = mockStore({
                fiveDaysForecast: {
                    cityId: 0,
                    isFetching: true,
                    isLoaded: false,
                    didInvalidate: false,
                    data: {}
                },
                selectedCity: { id: 0, name: 'test' }
            });
            store.dispatch(actions.fetchFiveDaysIfNeeded(store.dispatch, store.getState))
                .then(() => {
                    expect(store.getActions()).toEqual([]);
                    expect(fetch).toHaveBeenCalledTimes(0);
                });
        });
    });

    describe('cities actions', () => {
        // beforeEach(() => {
        //     jest.resetModules();
        // });

        it('creates RECIEVE_CITIES', () => {
            // window.require = jest.fn().mockImplementation(() => {
            //     return [{ id: 0, name: 'test' }];
            // });
            jest.mock('../../../constants/cityList.json', () => {
                return [{name: 'test'}];
            }); // it doesn't mock 

            const expectedActions = [
                { type: types.REQUEST_CITIES },
                { type: types.RECEIVE_CITIES }
            ];

            const store = mockStore({
                cities: {
                    isFetching: false,
                    didInvalidate: false,
                    data: []
                }
            });

            store.dispatch(actions.fetchCitiesIfNeeded(store.dispatch, store.getState))
                .then(() => {
                    expect(store.getActions()).toEqual(expectedActions);
                    expect(require).toHaveBeenCalledTimes(1);

                });
        });
    })
});
