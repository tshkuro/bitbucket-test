import * as actions from '../../../actions/weatherActions';
import types, {forecastTypes} from '../../../constants/weatherTypes';

describe('weather actions', () => {
    it('check action selectCity', () => {
        const id = 1;
        const name = 'test';
        const expectedAction = {
            type: types.SELECT_CITY,
            id,
            name
        };
        expect(actions.selectCity(id, name)).toEqual(expectedAction);
    });

    it('check action showForecast for current', () => {
        const expectedAction = {
            type: types.SHOW_CUR,
        };
        expect(actions.showForecast(forecastTypes.CURRENT))
            .toEqual(expectedAction);
    });

    it('check action showForecast for five days', () => {
        const expectedAction = {
            type: types.SHOW_FIVE_DAYS,
        };
        expect(actions.showForecast(forecastTypes.FIVE_DAYS))
            .toEqual(expectedAction);
    });

    it('check action showForecast for unknown type', () => {
        expect(() => actions.showForecast('something else')).toThrow();
    });

    // cities

    it('check action invalidateCities', () => {
        const expectedAction = {
            type: types.INVALIDATE_CITIES,
        };
        expect(actions.invalidateCities()).toEqual(expectedAction);
    });
    
    it('check action requestCities', () => {
        const expectedAction = {
            type: types.REQUEST_CITIES,
        };
        expect(actions.requestCities()).toEqual(expectedAction);
    });

    it('check action receiveCities', () => {
        const expectedAction = {
            type: types.RECEIVE_CITIES,
        };
        expect(actions.receiveCities()).toEqual(expectedAction);
    });

    // current forecast

    it('check action invalidateCurrent', () => {
        const expectedAction = {
            type: types.INVALIDATE_CURRENT,
        };
        expect(actions.invalidateCurrent()).toEqual(expectedAction);
    });
    
    it('check action requestCurrent', () => {
        const expectedAction = {
            type: types.REQUEST_CURRENT,
        };
        expect(actions.requestCurrent()).toEqual(expectedAction);
    });

    it('check action receiveCurrent', () => {
        const expectedAction = {
            type: types.RECEIVE_CURRENT,
        };
        expect(actions.receiveCurrent()).toEqual(expectedAction);
    });
    
    // five days forecast

    it('check action invalidateFiveDays', () => {
        const expectedAction = {
            type: types.INVALIDATE_DAYS,
        };
        expect(actions.invalidateFiveDays()).toEqual(expectedAction);
    });
    
    it('check action requestFiveDays', () => {
        const expectedAction = {
            type: types.REQUEST_DAYS,
        };
        expect(actions.requestFiveDays()).toEqual(expectedAction);
    });

    it('check action receiveFiveDays', () => {
        const expectedAction = {
            type: types.RECEIVE_DAYS,
        };
        expect(actions.receiveFiveDays()).toEqual(expectedAction);
    });
})