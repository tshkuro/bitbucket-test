import React from 'react';
import { shallow, mount } from 'enzyme';
import TasksNavbar, { TasksNavLink } from '../Navbar/Navbar';

class MockNavbarContainer {
    links = [
        { name: "Home", component: <div>Main</div>, id: "mainPage", link: "/" },
        { name: "Form", component: <div>Form</div>, id: "formPage", link: "/form" },
    ];
    current = 0;
    handleNavLinkClick = id => {
        this.current = this.links.findIndex(link => link.id === id);
    }
}

describe("Navbar component", () => {
    let wrapper;
    let mockContainer;

    beforeEach(() => {
        mockContainer = new MockNavbarContainer();
        wrapper = shallow(<TasksNavbar
            links={mockContainer.links}
            current={mockContainer.current}
            onClick={mockContainer.handleNavLinkClick}
        />);
    });

    it('should render components correctly', () => {
        expect(wrapper.find('Navbar')).toBeTruthy();
        expect(wrapper.find('Navbar').length).toBe(1);

        expect(wrapper.find('NavbarBrand')).toBeTruthy();
        expect(wrapper.find('NavbarBrand').length).toBe(1);

        expect(wrapper.find('NavbarToggle')).toBeTruthy();
        expect(wrapper.find('NavbarToggle').length).toBe(1);

        expect(wrapper.find('Nav')).toBeTruthy();
        expect(wrapper.find('Nav').length).toBe(1);

        expect(wrapper.find('TasksNavLink')).toBeTruthy();
        expect(wrapper.find('TasksNavLink').length).toBe(2);
    });
});

class MockNavLink {
    linkId = null;
    onClick = (id) => {
        this.linkId = id;
    }
}

describe("TasksNavLink component", () => {
    let wrapper;
    let mockLink;
    beforeEach(() => {
        mockLink = new MockNavLink();
        wrapper = shallow(
            <TasksNavLink
                id="1"
                link={{ link: "/link", id: "link1", name: "LinkName" }}
                onClick={mockLink.onClick}
            />);
    });

    it('should render NavLink correctly', () => {
        expect(wrapper.find('NavItem')).toBeTruthy();
        expect(wrapper.find('NavItem').length).toBe(1);

        expect(wrapper.find('Link')).toBeTruthy();
        expect(wrapper.find('Link').length).toBe(1);
    });

    it('has correct props', () => {
        let link = wrapper.find('Link');
        expect(link.prop('to')).toBe("/link");
        expect(link.prop('id')).toBe("link1");
        expect(link.prop('children')).toBe("LinkName");
    });

    it('onClick function works', () => {
        expect(mockLink.linkId).toBeFalsy();
        wrapper.find('Link').simulate('click');
        expect(mockLink.linkId).toBe('link1');
    });

})