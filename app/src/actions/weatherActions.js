// import fetch from 'cross-fetch'
import types, { forecastTypes } from '../constants/weatherTypes'
import link, { KEY } from '../constants/weatherLink'
import { getFormattedDay } from '../components/Weather/WeatherFormatting';

export const selectCity = (id, name) => {
    return {
        type: types.SELECT_CITY,
        id,
        name
    }
}

export const showForecast = (forecastType) => {
    switch (forecastType) {
        case forecastTypes.CURRENT:
            return {
                type: types.SHOW_CUR
            }
        case forecastTypes.FIVE_DAYS:
            return {
                type: types.SHOW_FIVE_DAYS
            }
        default:
            throw new Error('Unknown forecast type');
    }
}

export const updateForecast = (id, name, forecastType) => {
    return dispatch => {
        dispatch(selectCity(id, name));
        dispatch(showForecast(forecastType));
    }
}

// --- cities ---

export const invalidateCities = () => {
    return {
        type: types.INVALIDATE_CITIES
    }
}

export const requestCities = () => {
    return {
        type: types.REQUEST_CITIES
    }
}

export const receiveCities = data => {
    return {
        type: types.RECEIVE_CITIES,
        data
    }
}

const fetchCities = () => {
    let countries = ['RU']; //["RU", "US"];
    return dispatch => {
        dispatch(requestCities());
        return new Promise((resolve, reject) => {
            let data = require('../constants/city_list.json');
            console.log('CITIES', data.length)
            resolve(data);
        })
            .then(data => data.filter(city => countries.includes(city.country)));
    }
}

const shouldFetchCities = (state) => {
    const cities = state.cities;
    return (cities.data.length === 0 && !cities.isFetching);
}

export const fetchCitiesIfNeeded = () => {
    return (dispatch, getState) => {
        if (shouldFetchCities(getState())) {
            return dispatch(fetchCities());
        } else {
            return Promise.resolve();
        }
    }

}

// --- current forecast ---

export const invalidateCurrent = () => {
    return {
        type: types.INVALIDATE_CURRENT
    }
}

export const requestCurrent = cityId => {
    return {
        type: types.REQUEST_CURRENT,
        cityId
    }
}

export const receiveCurrent = data => {
    return {
        type: types.RECEIVE_CURRENT,
        data
    }
}

const fetchCurrent = cityId => {
    return dispatch => {
        dispatch(requestCurrent(cityId));
        let sublink = `weather?id=${cityId}&appid=${KEY}&units=metric&lang=ru`;
        return fetch(link + sublink)
            .then(
                response => {
                    return response.json()
                },
                error => console.log("Error in fetchCurrent.", error)
            )
            .then(json => {
                dispatch(receiveCurrent(json))
            })
            .catch(err => {
                dispatch(invalidateCurrent())
            });
    }
}

const shouldFetchCurrent = (state) => {
    const id = state.selectedCity.id;
    const cur = state.currentForecast;

    return (id !== -1 &&
        !cur.isFetching &&
        (!cur.isLoaded || cur.cityId !== id));
}

export const fetchCurrentIfNeeded = () => {
    return (dispatch, getState) => {
        const state = getState();
        if (shouldFetchCurrent(state)) {
            return dispatch(fetchCurrent(state.selectedCity.id));
        } else {
            return Promise.resolve();
        }

    }
}

// --- five days forecast ---

export const invalidateFiveDays = () => {
    return {
        type: types.INVALIDATE_DAYS
    }
}

export const requestFiveDays = cityId => {
    return {
        type: types.REQUEST_DAYS,
        cityId
    }
}

export const receiveFiveDays = data => {
    return {
        type: types.RECEIVE_DAYS,
        data
    }
}

const fetchFiveDays = cityId => {
    return dispatch => {
        dispatch(requestFiveDays(cityId));
        let sublink = `/forecast?id=${cityId}&appid=${KEY}&units=metric&lang=ru`;
        return fetch(link + sublink)
            .then(
                response => response.json(),
                error => console.log("Error in fetchCurrent.", error)
            )
            .then(json => divideIntoDays(json))
            .then(json => dispatch(receiveFiveDays(json)))
            .catch(dispatch(invalidateFiveDays()));
    }
}

const divideIntoDays = (json) => {
    let days = {};
    let daysCnt = 0;
    const daysLimit = 5;
    for (let item of json["list"]) {
        let date = getFormattedDay(item["dt"]);
        if (!(date in days)) {
            daysCnt++;
            if (daysCnt > daysLimit) {
                break;
            }
            days[date] = [];
        }
        days[date].push(item);
    }
    return days;
}

const shouldFetchFiveDays = (state) => {
    const id = state.selectedCity.id;
    const cur = state.fiveDaysForecast;
    return (id !== -1 &&
        !cur.isFetching &&
        (!cur.isLoaded || cur.cityId !== id));
}

export const fetchFiveDaysIfNeeded = () => {
    return (dispatch, getState) => {
        const state = getState();
        if (shouldFetchFiveDays(state)) {
            return dispatch(fetchFiveDays(state.selectedCity.id));
        } else {
            return Promise.resolve();
        }

    }
}
