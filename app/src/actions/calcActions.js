import types from '../constants/calculatorTypes';

export const addSymbol = symbol => ({
    type: types.ADD_SYMBOL,
    symbol
});

export const delSymbol = () => ({
    type: types.DEL_SYMBOL
});

export const clearAll = () => ({
    type: types.CLEAR_ALL
});

export const evaluate = () => ({
    type: types.EVAL
});



