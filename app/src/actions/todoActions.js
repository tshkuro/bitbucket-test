import types from '../constants/todolistTypes';
let nextTodoId = 0;

export const addTodo = text => ({
    type: types.ADD_ITEM,
    id: nextTodoId++,
    text
});

export const setVisibilityFilter = filter => ({
    type: types.FILTER,
    filter
});

export const toggleTodo = id => ({
    type: types.TOGGLE_ITEM,
    id
});

export const deleteTodo = id => ({
    type: types.DELETE_ITEM,
    id
});
