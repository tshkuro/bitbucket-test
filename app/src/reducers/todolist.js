import { combineReducers } from 'redux'
import types from '../constants/todolistTypes'
import filters from '../constants/filters'
import todoData from '../components/TodoList/todoData'

const todos = (state = todoData, action) => {
  switch (action.type) {
    case types.ADD_ITEM:
      return [
        ...state,
        {
          id: action.id,
          text: action.text,
          completed: false
        }
      ]
    case types.TOGGLE_ITEM:
      return state.map(todo =>
        todo.id === action.id ? { ...todo, completed: !todo.completed } : todo
      )
    case types.DELETE_ITEM:
      return state.filter(todo => todo.id !== action.id)
    default:
      return state
  }
}

const visibilityFilter = (state = filters.SHOW_ALL, action) => {
  switch (action.type) {
    case types.FILTER:
      return action.filter
    default:
      return state
  }
}

export default combineReducers({
  todos,
  visibilityFilter
})
