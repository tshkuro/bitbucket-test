import { combineReducers } from 'redux'
import types, { forecastTypes } from '../constants/weatherTypes'
/*
{
    selectedCity: {id, name},
    selectedForecast: type
    cities: {
        isFetching: bool,
        didInvalidate: bool,
        data: []
    }
    currentForecast: {
        isLoaded: bool,
        isFetching: bool,
        didInvalidate: bool,
        cityId,
        data: {}
    }
    fiveDaysForecast: {
        isLoaded: bool,
        isFetching: bool,
        didInvalidate: bool,
        cityId,
        data: {
            day0: {}
            day1: {}
            day2: {}
            day3: {}
            day4: {}
        }
    }
}
*/

const selectedCity = (state = {
    id: -1,
    name: ''
}, action) => {
    if (action.type === types.SELECT_CITY) {
        return {
            id: action.id,
            name: action.name
        }
    }
    return state
}

const selectedForecast = (
    state = forecastTypes.CURRENT,
    action) => {
    // console.log('reducer selectforecast', action.type);
    switch (action.type) {
        case types.SHOW_CUR:
            return forecastTypes.CURRENT;
        case types.SHOW_FIVE_DAYS:
            return forecastTypes.FIVE_DAYS;
        default:
            return state;
    }
}

const cities = (
    state = {
        isFetching: false,
        didInvalidate: false,
        data: []
    },
    action
) => {
    switch (action.type) {
        case types.INVALIDATE_CITIES:
            return {
                ...state,
                didInvalidate: true
            }
        case types.REQUEST_CITIES:
            return {
                ...state,
                isFetching: true,
                didInvalidate: false
            }
        case types.RECEIVE_CITIES:
            return {
                ...state,
                isFetching: false,
                didInvalidate: false,
                data: action.data
            }
        default:
            return state
    }
}

const currentForecast = (
    state = {
        isLoaded: false,
        isFetching: false,
        didInvalidate: false,
        cityId: -1,
        data: {}
    },
    action
) => {
    switch (action.type) {
        case types.INVALIDATE_CURRENT:
            return {
                ...state,
                didInvalidate: true
            }
        case types.REQUEST_CURRENT:
            return {
                ...state,
                isLoaded: false,
                isFetching: true,
                didInvalidate: false,
                cityId: action.cityId
            }
        case types.RECEIVE_CURRENT:
            return {
                ...state,
                isLoaded: true,
                isFetching: false,
                didInvalidate: false,
                data: action.data
            }
        default:
            return state
    }
}

const fiveDaysForecast = (
    state = {
        isLoaded: false,
        isFetching: false,
        didInvalidate: false,
        cityId: -1,
        data: {}
    },
    action
) => {
    switch (action.type) {
        case types.INVALIDATE_DAYS:
            return {
                ...state,
                didInvalidate: true
            }
        case types.REQUEST_DAYS:
            return {
                ...state,
                isLoaded: false,
                isFetching: true,
                didInvalidate: false,
                cityId: action.cityId
            }
        case types.RECEIVE_DAYS:
            return {
                ...state,
                isLoaded: true,
                isFetching: false,
                didInvalidate: false,
                data: action.data
            }
        default:
            return state
    }
}

export default combineReducers({
    selectedCity,
    selectedForecast,
    cities,
    currentForecast,
    fiveDaysForecast
})

/*
export const currentForecast = (state = {}, action) => {
    if (action.type === types.GET_CUR) {
        // load with action.cityId
    }
}

export const fiveDaysForecast = (state = {}, action) => {
    if (action.type === types.GET_DAYS) {
        // load with action.cityId
    }
}
*/
