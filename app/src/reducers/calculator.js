import types from '../constants/calculatorTypes'

export default (state = '0', action) => {
  switch (action.type) {
    case types.ADD_SYMBOL:
      return smartAdd(state, action.symbol);
    case types.DEL_SYMBOL:
      let s = state.toString();
      return s.slice(0, s.length - 1) || '0';
    case types.CLEAR_ALL:
      return '0';
    case types.EVAL:
      return smartEval(state);
    default:
      return state;
  }
}

function isDigit(char) {
  return char >= '0' && char <= '9';
}

function smartEval(state = '0') {
  let str = state.toString();
  let res = '';
  try {
    res = eval(str);
  } catch (e) {
    res = state;
  }
  return res.toString();
}

function smartAdd(state = '0', symbol) {
  let str = state.toString();
  if (str.length === 1 && str[0] === '0') {
    return symbol;
  }
  if (str.length >= 1) {
    if (/^.*\)$/.test(str) && (symbol === '(' || isDigit(symbol))) {
      return state;
    }
    if (/^.*[+\-*/]$/.test(str)) {
      if (!isDigit(symbol) && !/^.*[()]$/.test(symbol)) {
        return state;
      }
    }
    if (/^.*[\.]$/.test(str) && !isDigit(symbol)) {
      return state;
    }
  }
  return str + symbol;
}
