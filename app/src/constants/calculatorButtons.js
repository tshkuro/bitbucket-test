export default [
    { id: "btnLeftBr", text: "(" },
    { id: "btnRightBr", text: ")" },
    { id: "btnAC", text: "AC" },
    { id: "btnBS", text: "<=" },

    { id: "btn1", text: "1" },
    { id: "btn2", text: "2" },
    { id: "btn3", text: "3" },
    { id: "btnDiv", text: "/" },

    { id: "btn4", text: "4" },
    { id: "btn5", text: "5" },
    { id: "btn6", text: "6" },
    { id: "btnMul", text: "*" },

    { id: "btn7", text: "7" },
    { id: "btn8", text: "8" },
    { id: "btn9", text: "9" },
    { id: "btnSub", text: "-" },

    { id: "btn0", text: "0" },
    { id: "btnDot", text: "." },
    { id: "btnEquals", text: "=" },
    { id: "btnAdd", text: "+" }
];