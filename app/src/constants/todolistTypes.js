export default {
    ADD_ITEM: 'ADD_ITEM', 
    DELETE_ITEM: 'DELETE_ITEM',
    TOGGLE_ITEM: 'TOGGLE_ITEM',
    FILTER: 'FILTER',
};