export default {
    ADD_SYMBOL: 'ADD_SYMBOL',
    DEL_SYMBOL: 'DEL_SYMBOL',
    CLEAR_ALL: 'CLEAR_ALL',
    EVAL: 'EVAL'
};