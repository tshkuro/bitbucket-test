context("Authentication form", () => {
    Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        // failing the test
        return false
    });

    beforeEach(() => {
        cy.visit('/auth');
    });

    it('should show form (more like unit tests)', () => {
        cy.get('form')
            .should('exist')
            .and('have.length', 1);

        cy.get('form input')
            .should('have.length', 3);
        cy.get('form label')
            .should('have.length', 3);
        cy.get('form button')
            .should('exist')
            .and('have.length', 1)
            .and('have.text', 'Submit')
            .and('have.attr', 'disabled')

        cy.get('form label').eq(0)
            .should('contain.text', 'Email');
        cy.get('form label').eq(1)
            .should('contain.text', 'First name');
        cy.get('form label').eq(2)
            .should('contain.text', 'Last name');
    });

    it('should show message for invalid email', () => {
        cy.get('input').eq(0)
            .type('a')
            .should('have.class', 'fieldWrong');
        cy.get('#emailMessage')
            .should('exist')
            .and('contain.text', 'not an email');
    });

    it('should show message for invalid first name', () => {
        cy.get('input').eq(1)
            .type('a')
            .should('have.class', 'fieldWrong');
        cy.get('#firstNameMessage')
            .should('exist')
            .and('contain.text', 'must be longer than 3 symbols');
    });

    it('should show message for invalid last name', () => {
        cy.get('input').eq(2)
            .type('a')
            .should('have.class', 'fieldWrong');
        cy.get('#lastNameMessage')
            .should('exist')
            .and('contain.text', 'must be longer than 3 symbols');
    });

    it('should accept correct values, show success message, then hide it', () => {
        cy.get('form button')
            .should('have.attr', 'disabled');

        cy.get('form input').eq(0)
            .type('example@mail.com')
            .should('have.class', 'fieldCorrect');
        cy.get('form input').eq(1)
            .type('John')
            .should('have.class', 'fieldCorrect');
        cy.get('form input').eq(2)
            .type('Doe')
            .should('have.class', 'fieldCorrect');

        cy.get('form button')
            .should('not.have.attr', 'disabled');

        cy.get('form button')
            .click();

        cy.get('[datatest="submitResult"]')
            .should('exist');

        cy.get('form input').eq(0)
            .should('have.value', '');
        cy.get('form input').eq(1)
            .should('have.value', '');
        cy.get('form input').eq(2)
            .should('have.value', '');

        cy.wait(3000);
        cy.get('[datatest="submitResult"]').should('not.exist');

    })

});