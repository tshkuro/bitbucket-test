context("Home page", () => {
    Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        // failing the test
        return false
    });

    beforeEach(() => {
        cy.visit('/');
    });

    it('works', () => {
        cy.get('h3').should('contain.text', 'Welcome');
        cy.get('h4').should('exist');
    });

    it('is navigated using navbar', () => {
        cy.get('#task1').click();
        cy.url().should('contain', 'auth');

        cy.get('#task2').click();
        cy.url().should('contain', 'todo');

        cy.get('#task3').click();
        cy.url().should('contain', 'calc');

        cy.get('#task4').click();
        cy.url().should('contain', 'weather');
    });
});