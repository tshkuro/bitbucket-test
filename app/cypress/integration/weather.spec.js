const currentRes = {
    "coord": { "lon": 139, "lat": 35 },
    "weather": [
        {
            "id": 800,
            "main": "Clear",
            "description": "clear sky",
            "icon": "01n"
        }
    ],
    "base": "stations",
    "main": {
        "temp": 281.52,
        "feels_like": 278.99,
        "temp_min": 280.15,
        "temp_max": 283.71,
        "pressure": 1016,
        "humidity": 93
    },
    "wind": {
        "speed": 0.47,
        "deg": 107.538
    },
    "rain": {
        "rain.3h": 0,
    },
    "snow": {
        "3h": 0,
    },
    "clouds": {
        "all": 2
    },
    "dt": 1560350192,
    "sys": {
        "type": 3,
        "id": 2019346,
        "message": 0.0065,
        "country": "JP",
        "sunrise": 1560281377,
        "sunset": 1560333478
    },
    "timezone": 32400,
    "id": 1851632,
    "name": "Shuzenji",
    "cod": 200
};

const fiveDaysRes = {
    "cod": "200",
    "message": 0,
    "cnt": 40,
    "list": [
        {
            "dt": 1578409200,
            "main": {
                "temp": 284.92,
                "feels_like": 281.38,
                "temp_min": 283.58,
                "temp_max": 284.92,
                "pressure": 1020,
                "sea_level": 1020,
                "grnd_level": 1016,
                "humidity": 90,
                "temp_kf": 1.34
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 5.19,
                "deg": 211
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2020-01-07 15:00:00"
        },
        {
            "dt": 1578409200,
            "main": {
                "temp": 284.92,
                "feels_like": 281.38,
                "temp_min": 283.58,
                "temp_max": 284.92,
                "pressure": 1020,
                "sea_level": 1020,
                "grnd_level": 1016,
                "humidity": 90,
                "temp_kf": 1.34
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 5.19,
                "deg": 211
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2020-01-07 15:00:00"
        },
        {
            "dt": 1578495600,
            "main": {
                "temp": 284.92,
                "feels_like": 281.38,
                "temp_min": 283.58,
                "temp_max": 284.92,
                "pressure": 1020,
                "sea_level": 1020,
                "grnd_level": 1016,
                "humidity": 90,
                "temp_kf": 1.34
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 5.19,
                "deg": 211
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2020-01-07 15:00:00"
        },
        {
            "dt": 1578495600,
            "main": {
                "temp": 284.92,
                "feels_like": 281.38,
                "temp_min": 283.58,
                "temp_max": 284.92,
                "pressure": 1020,
                "sea_level": 1020,
                "grnd_level": 1016,
                "humidity": 90,
                "temp_kf": 1.34
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 5.19,
                "deg": 211
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2020-01-07 15:00:00"
        },
    ],

    "city": {
        "id": 2643743,
        "name": "London",
        "coord": {
            "lat": 51.5073,
            "lon": -0.1277
        },
        "country": "GB",
        "timezone": 0,
        "sunrise": 1578384285,
        "sunset": 1578413272
    }
}


context("Weather page", () => {
    Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        // failing the test
        return false
    });

    beforeEach(() => {
        cy.visit('/weather');
    });

    describe("unit tests", () => {
        it("shows components", () => {
            cy.get('[datatest="weatherHeaderComponent"]')
                .should('exist');
            cy.get('[datatest="weatherHeaderComponent"] h1')
                .should('exist')
                .and('contain.text', 'What is the weather?');
            cy.get('[datatest="citySearchComponent"]')
                .should('exist');
            cy.get('[datatest="citySelectComponent"]')
                .should('exist');
            cy.get('[datatest="typeSelectComponent"]')
                .should('exist');
        });
    });

    it("integration test with mocking for current forecast", () => {
        cy.server();
        cy.route({
            method: "GET",
            url: "**/weather*",
            response: currentRes
        });

        cy.get('[datatest="citySelectComponent"] select > option')
            .eq(1).then(element => {
                cy.get('[datatest="citySelectComponent"] select')
                    .select(element.val());
            });

        // city name
        cy.get('[datatest="citySelectComponent"] select > option')
            .eq(1).then(element => {
                cy.get('[datatest="weatherHeaderComponent"] h3')
                    .should('exist')
                    .and('contain.text', element.text());
            });
        // weather panel
        cy.get('[datatest="currentForecastView"]')
            .should('exist');
        // panel header
        cy.get('[datatest="currentForecastView"] h2')
            .should('exist')
            .and('contain.text', 'Сегодня');
        // panel time
        cy.get('[datatest="currentForecastView"] h4')
            .should('exist')
            .and('contain.text', 'Текущее время:');
        // panel weather description
        cy.get('[datatest="currentForecastView"] h3')
            .should('exist')
            .find('img').should('exist')

        // temperature widget
        cy.get('[datatest="temperatureWidget"]')
            .should('exist');
        // pressure widget
        cy.get('[datatest="pressureWidget"]')
            .should('exist');
        // wind widget
        cy.get('[datatest="windWidget"]')
            .should('exist');
        // rain widget
        cy.get('[datatest="rainWidget"]')
            .should('exist');
        // snow widget
        cy.get('[datatest="snowWidget"]')
            .should('exist');
    });

    it("integration test with mocking for five days forecast", () => {
        cy.server();
        cy.route({
            method: "GET",
            url: "**/forecast*",
            response: fiveDaysRes
        });

        cy.get('[datatest="typeSelectComponent"] select ').select('Five days')

        cy.get('[datatest="citySelectComponent"] select > option')
            .eq(1).then(element => {
                cy.get('[datatest="citySelectComponent"] select')
                    .select(element.val());
            });

        // city name
        cy.get('[datatest="citySelectComponent"] select > option')
            .eq(1).then(element => {
                cy.get('[datatest="weatherHeaderComponent"] h3')
                    .should('exist')
                    .and('contain.text', element.text());
            });

        // weather panel
        cy.get('[datatest="fiveDaysForecastView"]')
            .should('exist');

        // day tabs
        cy.get('[datatest="dayTab"]')
            .should('exist')
            .and('have.length', 2);

        // panel header
        cy.get('[datatest="tabHeader"]')
            .should('exist')
            .and('have.length', 2)
        cy.get('[datatest="tabHeader"]').eq(0)
            .should('contain.text', 'Сегодня');

        cy.get('[datatest="panelRow"]')
            .should('exist')
            .and('have.length', 4);

        cy.get('[datatest="panelRow"]').each(row => {
            // time
            cy.wrap(row).find('[datatest="timeWidget"]')
                .should('exist');
            // description
            cy.wrap(row).find('[datatest="descriptionWidget"]')
                .should('exist');
            // temperature
            cy.wrap(row).find('[datatest="temperatureWidget"]')
                .should('exist');
            // pressure
            cy.wrap(row).find('[datatest="pressureWidget"]')
                .should('exist');
            // wind
            cy.wrap(row).find('[datatest="windWidget"]')
                .should('exist');
        })
    });
});