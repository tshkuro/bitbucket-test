const black = 'rgb(33, 37, 41)';
const gray = 'rgb(128, 128, 128)';

context("Authentication form", () => {
    Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        // failing the test
        return false
    });

    beforeEach(() => {
        cy.visit('/todo');
    });

    it('shows todo list (unit tests)', () => {
        cy.get('.filters')
            .should('exist');

        cy.get('.filters button')
            .should('exist')
            .and('have.length', 3);

        cy.get('h3')
            .should('exist')
            .should('contain.text', 'TODO list');

        cy.get('input[datatest="addItemInput"]')
            .should('exist')
            .and('have.length', 1);

        cy.get('button[datatest="addItemButton"')
            .should('exist')
            .and('have.length.greaterThan', 0);
    });

    describe('integration tests for todo', () => {
        beforeEach(() => {
            cy.get('input[datatest="addItemInput"]')
                .type('Test todo');
            cy.get('button[datatest="addItemButton"')
                .click();
        });

        it('has added todo item', () => {
            cy.get('[datatest="todoItemComponent"]:last-child')
                .should('exist')

            cy.get('[datatest="todoItemComponent"]:last-child label')
                .should('exist')
                .and('have.text', 'Test todo');

            cy.get('[datatest="todoItemComponent"]:last-child input[type="checkbox"]')
                .should('exist')

            cy.get('[datatest="todoItemComponent"]:last-child button')
                .should('exist')
        });

        it('can toggle todo checkbox', () => {
            cy.get('[datatest="todoItemComponent"]:last-child label')
                .should('have.css', 'color', black);

            cy.get('[datatest="todoItemComponent"]:last-child input[type="checkbox"]')
                .should('not.be.checked')
                .check({ force: true })
                .should('be.checked');

            cy.get('[datatest="todoItemComponent"]:last-child label')
                .should('have.css', 'color', gray);

            cy.get('[datatest="todoItemComponent"]:last-child input[type="checkbox"]')
                .uncheck({ force: true })
                .should('not.be.checked');

            cy.get('[datatest="todoItemComponent"]:last-child label')
                .should('have.css', 'color', black);
        });

        it('can delete todo', () => {
            cy.get('[datatest="todoItemComponent"]').its('length').as('prevLength');
            // console.log(this.todoCnt)
            cy.get('[datatest="todoItemComponent"]:last-child button')
                .click();
            cy.get('@prevLength')
                .then(($prevLength) => {
                    cy.get('[datatest="todoItemComponent"]')
                        .should('have.length', $prevLength - 1)
                });
        });
    });

    describe('integration tests for filters', () => {
        beforeEach(() => {
            cy.get('[datatest="todoItemComponent"] button')
                .each(($el) => $el.click());

            // add todos
            cy.get('input[datatest="addItemInput"]')
                .type('Test todo 1');
            cy.get('button[datatest="addItemButton"')
                .click();
            cy.get('input[datatest="addItemInput"]')
                .type('Test todo 2');
            cy.get('button[datatest="addItemButton"')
                .click();

            // check 'Test todo 2' item
            cy.get('[datatest="todoItemComponent"]:last-child input[type="checkbox"]')
                .check({ force: true })
        });

        it('should show all items for "All" filter', () => {
            cy.get('.filters button').eq(0)
                .should('exist')
                .and('contain.text', 'All');
            cy.get('[datatest="todoItemComponent"]')
                .should('have.length', 2);
        });

        it('should show unchecked items for "Active" filter', () => {
            cy.get('.filters button').eq(1)
                .should('exist')
                .and('contain.text', 'Active')
                .click();
            cy.get('[datatest="todoItemComponent"]')
                .should('have.length', 1);
            cy.get('[datatest="todoItemComponent"] label')
                .should('contain.text', 'Test todo 1');
            cy.get('[datatest="todoItemComponent"] input[type="checkbox"]')
                .should('not.be.checked');
        });

        it('should show checked items for "Completed" filter', () => {
            cy.get('.filters button').eq(2)
                .should('exist')
                .and('contain.text', 'Completed')
                .click();
            cy.get('[datatest="todoItemComponent"]')
                .should('have.length', 1);
            cy.get('[datatest="todoItemComponent"] label')
                .should('contain.text', 'Test todo 2');
            cy.get('[datatest="todoItemComponent"] input[type="checkbox"]')
                .should('be.checked');
        });

    })


});