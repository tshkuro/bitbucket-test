context("Calculator page", () => {
    Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        // failing the test
        return false
    });

    beforeEach(() => {
        cy.visit('/calc');
    });

    it('should show components', () => {
        cy.get('[datatest="calculatorComponent"]')
            .should('exist');
        cy.get('#calcInput')
            .should('exist')
            .and('contain.value', 0);
        cy.get('#btnLeftBr')
            .should('exist')
            .and('have.length', 1)
            .and('contain.text', '(');
        cy.get('#btnRightBr')
            .should('exist')
            .and('have.length', 1)
            .and('contain.text', ')');
        cy.get('#btnAC')
            .should('exist')
            .and('have.length', 1)
            .and('contain.text', 'AC');
        cy.get('#btnBS')
            .should('exist')
            .and('have.length', 1)
            .and('contain.text', '<=');

        cy.get('#btn0')
            .should('exist')
            .and('have.length', 1)
            .and('contain.text', '0');
        cy.get('#btn1')
            .should('exist')
            .and('have.length', 1)
            .and('contain.text', '1');
        cy.get('#btn2')
            .should('exist')
            .and('have.length', 1)
            .and('contain.text', '2');
        cy.get('#btn3')
            .should('exist')
            .and('have.length', 1)
            .and('contain.text', '3');
        cy.get('#btn4')
            .should('exist')
            .and('have.length', 1)
            .and('contain.text', '4');
        cy.get('#btn5')
            .should('exist')
            .and('have.length', 1)
            .and('contain.text', '5');
        cy.get('#btn6')
            .should('exist')
            .and('have.length', 1)
            .and('contain.text', '6');
        cy.get('#btn7')
            .should('exist')
            .and('have.length', 1)
            .and('contain.text', '7');
        cy.get('#btn8')
            .should('exist')
            .and('have.length', 1)
            .and('contain.text', '8');
        cy.get('#btn9')
            .should('exist')
            .and('have.length', 1)
            .and('contain.text', '9');

        cy.get('#btnDot')
            .should('exist')
            .and('have.length', 1)
            .and('contain.text', '.');
        cy.get('#btnEquals')
            .should('exist')
            .and('have.length', 1)
            .and('contain.text', '=');
        cy.get('#btnDiv')
            .should('exist')
            .and('have.length', 1)
            .and('contain.text', '/');
        cy.get('#btnMul')
            .should('exist')
            .and('have.length', 1)
            .and('contain.text', '*');
        cy.get('#btnSub')
            .should('exist')
            .and('have.length', 1)
            .and('contain.text', '-');
        cy.get('#btnAdd')
            .should('exist')
            .and('have.length', 1)
            .and('contain.text', '+');

    });

    describe('input works correctly', () => {
        it('input numbers and braces', () => {
            // input numbers and braces
            cy.get('#btnLeftBr').click();
            cy.get('#btn1').click();
            cy.get('#btn2').click();
            cy.get('#btnRightBr').click();

            cy.get('#calcInput')
                .should('have.value', '(12)');
        });

        it('AC clears input', () => {
            cy.get('#btnAC').click();
            cy.get('#calcInput')
                .should('have.value', '0');
        });

        it('can delete symbols using backspace button', () => {
            cy.get('#btn1').click();
            cy.get('#btn2').click();

            cy.get('#calcInput')
                .should('have.value', '12');
            cy.get('#btnBS').click();
            cy.get('#calcInput')
                .should('have.value', '1');
        });

        it('dot button works', () => {
            cy.get('#btn1').click();
            cy.get('#btnDot').click();
            cy.get('#btn2').click();
            cy.get('#calcInput')
                .should('have.value', '1.2');
        });

        it('add button works', () => {
            cy.get('#btn1').click();
            cy.get('#btnAdd').click();
            cy.get('#btn2').click();
            cy.get('#calcInput')
                .should('have.value', '1+2');
        });

        it('sub button works', () => {
            cy.get('#btn1').click();
            cy.get('#btnSub').click();
            cy.get('#btn2').click();
            cy.get('#calcInput')
                .should('have.value', '1-2');
        });

        it('mul button works', () => {
            cy.get('#btn1').click();
            cy.get('#btnMul').click();
            cy.get('#btn2').click();
            cy.get('#calcInput')
                .should('have.value', '1*2');
        });

        it('div button works', () => {
            cy.get('#btn1').click();
            cy.get('#btnDiv').click();
            cy.get('#btn2').click();
            cy.get('#calcInput')
                .should('have.value', '1/2');
        });
    });

    describe('simple operations tests', () => {
        it('can add numbers', () => {
            cy.get('#btn2').click();
            cy.get('#btnAdd').click();
            cy.get('#btn3').click();
            cy.get('#btnEquals').click();

            cy.get('#calcInput')
                .should('have.value', '5');
        });

        it('can subtract numbers', () => {
            cy.get('#btn6').click();
            cy.get('#btnSub').click();
            cy.get('#btn2').click();
            cy.get('#btnEquals').click();

            cy.get('#calcInput')
                .should('have.value', '4');
        });

        it('can multiply numbers', () => {
            cy.get('#btn2').click();
            cy.get('#btnMul').click();
            cy.get('#btn3').click();
            cy.get('#btnEquals').click();

            cy.get('#calcInput')
                .should('have.value', '6');
        });

        it('can divide numbers', () => {
            cy.get('#btn9').click();
            cy.get('#btnDiv').click();
            cy.get('#btn3').click();
            cy.get('#btnEquals').click();

            cy.get('#calcInput')
                .should('have.value', '3');
        });

        it('can work with negatives', () => {
            cy.get('#btnSub').click();
            cy.get('#btn2').click();
            cy.get('#btnAdd').click();
            cy.get('#btn1').click();
            cy.get('#btnEquals').click();

            cy.get('#calcInput')
                .should('have.value', '-1');
        })
    });

    describe("complex operations tests (not really necessary, because it's js' functionality)", () => {
        it('(1+2)*3', () => {
            cy.get('#btnLeftBr').click();
            cy.get('#btn1').click();
            cy.get('#btnAdd').click();
            cy.get('#btn2').click();
            cy.get('#btnRightBr').click();
            cy.get('#btnMul').click();
            cy.get('#btn3').click();
            
            cy.get('#btnEquals').click();

            cy.get('#calcInput')
                .should('have.value', '9');
        });

        it('(5+7)/3', () => {
            cy.get('#btnLeftBr').click();
            cy.get('#btn5').click();
            cy.get('#btnAdd').click();
            cy.get('#btn7').click();
            cy.get('#btnRightBr').click();
            cy.get('#btnDiv').click();
            cy.get('#btn3').click();
            
            cy.get('#btnEquals').click();

            cy.get('#calcInput')
                .should('have.value', '4');
        });
               
    });


});